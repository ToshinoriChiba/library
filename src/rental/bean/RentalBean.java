package rental.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * RentalのJavaBean
 *
 * @author Ryoki Morohashi
 */
public class RentalBean implements Serializable {

	/**
	 * 貸出ID
	 */
	private int rentalId;
	/**
	 * 図書ID
	 */
	private int bookStateId;
	/**
	 * 会員ID
	 */
	private int userId;
	/**
	 * 貸出日
	 */
	private Date rentalDay;
	/**
	 * 返却期限
	 */
	private Date limitDay;
	/**
	 * 返却日
	 */
	private Date returnDay;
	/**
	 * 備考
	 */
	private String remark;

	/**
	 * RentalBeanのコンストラクタ(値入力)
	 *
	 * @param rental_id
	 * @param book_state_id
	 * @param user_id
	 * @param rental_day
	 * @param limit_day
	 * @param return_day
	 * @param remark
	 */
	public RentalBean(int rental_id, int book_state_id, int user_id, Date rental_day, Date limit_day, Date return_day,
			String remark) {
		setRentalId(rental_id);
		setBookStateId(book_state_id);
		setUserId(user_id);
		setRentalDay(rental_day);
		setLimitDay(limit_day);
		setReturnDay(return_day);
		setRemark(remark);
	}

	/**
	 * RentalBeanのコンストラクタ(無条件)
	 */
	public RentalBean() {
		super();
	}

	// 以下、各フィールドのアクセッサー
	public int getRentalId() {
		return rentalId;
	}

	public void setRentalId(int rentalId) {
		this.rentalId = rentalId;
	}

	public int getBookStateId() {
		return bookStateId;
	}

	public void setBookStateId(int bookStateId) {
		this.bookStateId = bookStateId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getRentalDay() {
		return rentalDay;
	}

	public void setRentalDay(Date rentalDay) {
		this.rentalDay = rentalDay;
	}

	public Date getLimitDay() {
		return limitDay;
	}

	public void setLimitDay(Date limitDay) {
		this.limitDay = limitDay;
	}

	public Date getReturnDay() {
		return returnDay;
	}

	public void setReturnDay(Date returnDay) {
		this.returnDay = returnDay;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
