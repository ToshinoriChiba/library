package rental.bean;

import java.util.Calendar;
import java.util.Date;

import book.bean.BookBean;
import book.bean.BookInfoBean;
import book.bean.BookStateBean;
import book.dao.BookInfoDAO;
import book.dao.BookStateDAO;
import book.dao.CategoryDAO;
import book.dao.PublisherDAO;
import exception.DAOException;
import rental.dao.RentalDao;
import user.dao.UserDAO;

/**
 * 主にフォームなどから得た情報にエラーがないかチェックするためのクラス
 *
 * @author Ryoki Morohashi
 *
 */
public class CheckInputData {

	/**
	 * チェックを通す会員ID
	 */
	private int checkUserId;
	/**
	 * チェックを通す図書ID
	 */
	private int checkBookId;
	/**
	 * エラー時に表示するエラー文
	 */
	private String error;

	/**
	 * 貸出要求のあった本が現在他の方に貸し出されていないか、または蔵書に存在するかをチェックする
	 *
	 * @throws DAOException
	 */
	public void checkBook() throws DAOException {
		RentalDao check = new RentalDao();
		BookStateDAO size = new BookStateDAO();
		if (check.checkBookHistory(this.checkBookId) == 0) {
		} else {
			error = "この本は既に貸し出されています";
		}
		if (this.checkBookId == -1) {
		} else {
			if (size.selectByBookIdReturnSize(this.checkBookId) == 1) {
			} else {
				error = "この本は蔵書にありません";
			}
		}
	}

	/**
	 * 貸出を要求した会員に返却期日を過ぎて返していないものがないかをチェックする
	 *
	 * @throws DAOException
	 */
	public void checkOverLimit() throws DAOException {
		RentalDao check = new RentalDao();
		if (check.checkUserOverLimit(this.checkUserId) == 0) {
		} else {
			error = "この会員は借りている本の中に返却期日を過ぎているものがあります";
		}
	}

	/**
	 * 会員が現在借りている冊数を返す
	 *
	 * @return int:借りている冊数
	 * @throws DAOException
	 */
	public int userLendingNow() throws DAOException {
		RentalDao check = new RentalDao();
		return check.userLendCount(this.checkUserId);
	}

	/**
	 * 会員が返却した本が蔵書に存在するか、その会員に貸し出している本なのかを返す
	 *
	 * @throws DAOException
	 */
	public void checkRecieveData() throws DAOException {
		RentalDao check = new RentalDao();
		BookStateDAO size = new BookStateDAO();

		if (size.selectByBookIdReturnSize(this.checkBookId) == 1) {
		} else {
			error = "この本は蔵書にありません";
		}
		if (check.checkRecieve(this.checkUserId, this.checkBookId) == 1) {

		} else {
			error = "この本はこの会員に貸出中ではありません";
		}
	}

	/**
	 * 貸し出す本の返却期日が10日か15日かを返す
	 *
	 * @param bookid
	 * @return 10:新刊である 15:新刊でない
	 * @throws DAOException
	 */
	public int ReturnLimit(int bookid) throws DAOException {
		BookStateDAO bs = new BookStateDAO();
		BookStateBean book = bs.selectByBookId(bookid);
		BookInfoDAO bi = new BookInfoDAO();
		BookInfoBean bookinfo = bi.selectByISBN(book.getIsbn());
		Date day = bookinfo.getPublicationDay();
		Calendar cal = Calendar.getInstance();
		Calendar caltoday = Calendar.getInstance();
		cal.setTime(day);
		cal.add(Calendar.MONTH, 3);
		if (cal.before(caltoday)) {
			return 15;
		} else {
			return 10;
		}

	}

	/**
	 * String型をInt型に変換できるのか判定する
	 *
	 * @param id
	 * @return true:変換可能 false:変換不可能
	 */
	public boolean checkInt(String id) {
		try {
			Integer.parseInt(id);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * 入力データがint型に変換できるか判定する
	 *
	 * @param id
	 */
	public void formCheck(String id) {
		if (checkInt(id)) {
		} else {
			this.error = "IDは半角英数字で入力してください";
		}
	}

	/**
	 * 確認画面で本の情報を表示する際に必要な情報をBookidから集める
	 *
	 * @param bookid
	 *            図書情報が欲しい図書ID
	 * @return BookBean:必要情報
	 * @throws DAOException
	 */
	public BookBean ReturnBookInfo(int bookid) throws DAOException {
		BookStateDAO bs = new BookStateDAO();
		BookStateBean book = bs.selectByBookId(bookid);
		BookInfoDAO bi = new BookInfoDAO();
		BookInfoBean bookinfo = bi.selectByISBN(book.getIsbn());
		CategoryDAO ctg = new CategoryDAO();
		String category = ctg.selectByCategoryCode(bookinfo.getCategoryCode());
		PublisherDAO pb = new PublisherDAO();
		String pbl = pb.selectByPublisherCode(bookinfo.getPublisherCode());
		BookBean bk = new BookBean(bookid, book.getIsbn(), category, bookinfo.getBookName(), bookinfo.getAuthor(), pbl,
				bookinfo.getPublicationDay(), book.getArrivalDate(), book.getDisposalDate(), book.getRemark());
		return bk;
	}

	/**
	 * 会員が退会しているかどうかを判定する
	 *
	 * @throws DAOException
	 */
	public void UserLeaveCheck() throws DAOException {
		UserDAO lv = new UserDAO();
		if (lv.selectByUserReturnLeave(this.checkUserId) == 1) {
		} else {
			error = "この会員は存在しないか既に退会しています";
		}
	}

	/**
	 * CheckInputDataのコンストラクタ(無条件)
	 */
	public CheckInputData() {
		super();
	}

	/**
	 * CheckInputDataのコンストラクタ(値入力)
	 *
	 * @param useid
	 * @param bookid
	 */
	public CheckInputData(int useid, int bookid) {
		super();
		this.checkUserId = useid;
		this.checkBookId = bookid;
	}

	// 以下アクセッサー
	public int getCheckUserId() {
		return checkUserId;
	}

	public void setCheckUserId(int checkUserId) {
		this.checkUserId = checkUserId;
	}

	public int getCheckBookId() {
		return checkBookId;
	}

	public void setCheckBookId(int checkBookId) {
		this.checkBookId = checkBookId;
	}

	public String getError() {
		return error;
	}
}
