package rental.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import book.dao.DBpass;
import exception.DAOException;
import rental.bean.RentalBean;

/**
 * 貸出返却履歴のデータベースを使用する際に使うクラス
 *
 * @author Ryoki Morohashi
 */
public class RentalDao {

	private Connection con;

	/**
	 * このクラスのコンストラクタ
	 *
	 * @throws DAOException
	 */
	public RentalDao() throws DAOException {
		this.getConnection();
	}

	/**
	 * 図書IDから履歴を検索し返却されていないものがあるかを返す
	 *
	 * @param bookId
	 * @return 0:存在しない 1:存在する
	 * @throws DAOException
	 */
	public int checkBookHistory(int bookId) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id from rental where book_state_id=? and return_day is null";
			st = this.con.prepareStatement(sql);
			st.setInt(1, bookId);
			rs = st.executeQuery();
			List<Integer> list = new ArrayList<Integer>();
			while (rs.next()) {
				int id = rs.getInt("rental_id");
				list.add(id);
			}
			return list.size();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員IDから履歴を返却し返却期日を過ぎているものがあるかを返す
	 *
	 * @param userId
	 * @return 0:存在しない 1:存在する
	 * @throws DAOException
	 */
	public int checkUserOverLimit(int userId) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id from rental where (user_id=?) and (return_day is null) and (limit_day<current_date)";
			st = this.con.prepareStatement(sql);
			st.setInt(1, userId);
			rs = st.executeQuery();
			List<Integer> list = new ArrayList<Integer>();
			while (rs.next()) {
				int id = rs.getInt("rental_id");
				list.add(id);
			}
			return list.size();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員IDから履歴を検索し会員が現在借りている冊数を返す
	 *
	 * @param userId
	 * @return int:借りている冊数
	 * @throws DAOException
	 */
	public int userLendCount(int userId) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id from rental where (user_id=?) and (return_day is null)";
			st = this.con.prepareStatement(sql);
			st.setInt(1, userId);
			rs = st.executeQuery();
			List<Integer> list = new ArrayList<Integer>();
			while (rs.next()) {
				int id = rs.getInt("rental_id");
				list.add(id);
			}
			return list.size();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員に図書を貸出し、会員IDと図書IDを貸出履歴に記入する またこのとき何日貸し出すのかは参照値を使う
	 *
	 * @param userId
	 * @param bookId
	 * @param plusday
	 *            図書を貸し出す期間
	 * @return int:追加した貸出冊数(使用しない)
	 * @throws DAOException
	 */
	public int insert(int userId, int bookId, int plusday) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		try {
			String sql = "insert into rental(book_state_id,user_id,limit_day) values(?,?,current_date+?)";
			st = this.con.prepareStatement(sql);
			st.setInt(1, bookId);
			st.setInt(2, userId);
			st.setInt(3, plusday);
			int rows = st.executeUpdate();
			return rows;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました。");
		} finally {
			try {
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 貸出履歴を全件検索する
	 *
	 * @return List:検索結果
	 * @throws DAOException
	 */
	public List<RentalBean> searchAll() throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id,book_state_id,user_id,rental_day,limit_day,return_day,remark from rental";
			st = this.con.prepareStatement(sql);
			rs = st.executeQuery();
			List<RentalBean> list = new ArrayList<RentalBean>();
			while (rs.next()) {
				int rentalId = rs.getInt("rental_id");
				int bookId = rs.getInt("book_state_id");
				int userId = rs.getInt("user_id");
				Date rentalDay = rs.getDate("rental_day");
				Date limitDay = rs.getDate("limit_day");
				Date returnDay = rs.getDate("return_day");
				String reMark = rs.getString("remark");
				RentalBean bean = new RentalBean(rentalId, bookId, userId, rentalDay, limitDay, returnDay, reMark);
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員IDを検索条件として貸出履歴を検索する
	 *
	 * @param userid
	 * @return List:検索結果
	 * @throws DAOException
	 */
	public List<RentalBean> searchUser(int userid) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id,book_state_id,user_id,rental_day,limit_day,return_day,remark from rental where user_id=?";
			st = this.con.prepareStatement(sql);
			st.setInt(1, userid);
			rs = st.executeQuery();
			List<RentalBean> list = new ArrayList<RentalBean>();
			while (rs.next()) {
				int rentalId = rs.getInt("rental_id");
				int bookId = rs.getInt("book_state_id");
				int userId = rs.getInt("user_id");
				Date rentalDay = rs.getDate("rental_day");
				Date limitDay = rs.getDate("limit_day");
				Date returnDay = rs.getDate("return_day");
				String reMark = rs.getString("remark");
				RentalBean bean = new RentalBean(rentalId, bookId, userId, rentalDay, limitDay, returnDay, reMark);
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 図書IDを検索条件として貸出履歴を検索する
	 *
	 * @param bookid
	 * @return List:検索結果
	 * @throws DAOException
	 */
	public List<RentalBean> searchBook(int bookid) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id,book_state_id,user_id,rental_day,limit_day,return_day,remark from rental where book_state_id=?";
			st = this.con.prepareStatement(sql);
			st.setInt(1, bookid);
			rs = st.executeQuery();
			List<RentalBean> list = new ArrayList<RentalBean>();
			while (rs.next()) {
				int rentalId = rs.getInt("rental_id");
				int bookId = rs.getInt("book_state_id");
				int userId = rs.getInt("user_id");
				Date rentalDay = rs.getDate("rental_day");
				Date limitDay = rs.getDate("limit_day");
				Date returnDay = rs.getDate("return_day");
				String reMark = rs.getString("remark");
				RentalBean bean = new RentalBean(rentalId, bookId, userId, rentalDay, limitDay, returnDay, reMark);
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員IDと図書IDを検索条件として貸出履歴を検索する
	 *
	 * @param userid
	 * @param bookid
	 * @return List:貸出履歴
	 * @throws DAOException
	 */
	public List<RentalBean> searchDouble(int userid, int bookid) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id,book_state_id,user_id,rental_day,limit_day,return_day,remark from rental where user_id=? and book_state_id=?";
			st = this.con.prepareStatement(sql);
			st.setInt(1, userid);
			st.setInt(2, bookid);
			rs = st.executeQuery();
			List<RentalBean> list = new ArrayList<RentalBean>();
			while (rs.next()) {
				int rentalId = rs.getInt("rental_id");
				int bookId = rs.getInt("book_state_id");
				int userId = rs.getInt("user_id");
				Date rentalDay = rs.getDate("rental_day");
				Date limitDay = rs.getDate("limit_day");
				Date returnDay = rs.getDate("return_day");
				String reMark = rs.getString("remark");
				RentalBean bean = new RentalBean(rentalId, bookId, userId, rentalDay, limitDay, returnDay, reMark);
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員IDと図書IDから会員と返却しようとしている図書が一致する履歴があるかを返す
	 *
	 * @param userid
	 * @param bookid
	 * @return 0:存在しない 1:存在する
	 * @throws DAOException
	 */
	public int checkRecieve(int userid, int bookid) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id from rental where book_state_id=? and user_id=? and return_day is null";
			st = this.con.prepareStatement(sql);
			st.setInt(1, bookid);
			st.setInt(2, userid);
			rs = st.executeQuery();
			List<Integer> list = new ArrayList<Integer>();
			while (rs.next()) {
				int id = rs.getInt("rental_id");
				list.add(id);
			}
			return list.size();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 正常に返却された図書の貸出履歴の返却日を記入する
	 *
	 * @param userId
	 * @param bookId
	 * @return 返却した冊数(使用しない)
	 * @throws DAOException
	 */
	public int recieveBook(int userId, int bookId) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		try {
			String sql = "update rental set return_day=current_date where user_id=? and book_state_id=? and return_day is null";
			st = this.con.prepareStatement(sql);
			st.setInt(1, userId);
			st.setInt(2, bookId);
			int rows = st.executeUpdate();
			return rows;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました。");
		} finally {
			try {
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 図書IDと貸出IDで検索した貸出履歴に対して返却前のものに備考欄に記入します
	 *
	 * @param userId
	 * @param bookId
	 * @param remark
	 * @return int:変更した行数(使用しない)
	 * @throws DAOException
	 */
	public int updateRemark(int userId, int bookId, String remark) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		try {
			String sql = "update rental set remark=? where user_id=? and book_state_id=? and return_day is null";
			st = this.con.prepareStatement(sql);
			st.setString(1, remark);
			st.setInt(2, userId);
			st.setInt(3, bookId);
			int rows = st.executeUpdate();
			return rows;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました。");
		} finally {
			try {
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 貸出IDで検索した履歴の備考欄に記入する
	 *
	 * @param rentalId
	 * @param remark
	 * @return int:変更した行数(使用しない)
	 * @throws DAOException
	 */
	public int updateRemarkRecieve(int rentalId, String remark) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		try {
			String sql = "update rental set remark=? where rental_id=?";
			st = this.con.prepareStatement(sql);
			st.setString(1, remark);
			st.setInt(2, rentalId);
			int rows = st.executeUpdate();
			return rows;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました。");
		} finally {
			try {
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * 会員IDと図書IDで検索した履歴の中から 本日返却されているものの中から最新のものの貸出IDを返す
	 *
	 * @param userid
	 * @param bookid
	 * @return int:貸出ID
	 * @throws DAOException
	 */
	public int RecieveId(int userid, int bookid) throws DAOException {
		if (this.con == null) {
			this.getConnection();
		}
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			String sql = "select rental_id from rental where book_state_id=? and user_id=? and return_day=current_date order by rental_id desc limit 1";
			st = this.con.prepareStatement(sql);
			st.setInt(1, bookid);
			st.setInt(2, userid);
			rs = st.executeQuery();
			int id = 0;
			while (rs.next()) {
				id = rs.getInt("rental_id");
			}
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (st != null)
					st.close();
				this.close();
			} catch (Exception e) {
				throw new DAOException("リソースの開放に失敗しました。");
			}
		}
	}

	/**
	 * DBに対してログインするためのメソッド
	 *
	 * @throws DAOException
	 */
	private void getConnection() throws DAOException {
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql:library";
			String user = "postgres";
			String pass = (new DBpass()).getDBpass();
			this.con = DriverManager.getConnection(url, user, pass);
		} catch (Exception e) {
			throw new DAOException("接続に失敗しました。");
		}
	}

	/**
	 * エラー発生時にconnectionをcloseするためのメソッド
	 *
	 * @throws SQLException
	 */
	private void close() throws SQLException {
		if (con != null) {
			con.close();
			con = null;
		}

	}

}
