package rental.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import rental.bean.CheckInputData;
import rental.bean.RentalBean;
import rental.dao.RentalDao;

/**
 * 検索作業のServlet
 *
 * @author Ryoki Morohashi
 */
@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SearchServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			CheckInputData check = new CheckInputData();
			String useId = request.getParameter("userId");
			if (useId.equals("")) {
			} else {
				check.formCheck(useId);
			}
			if (check.getError() == null) {
			} else {
				request.setAttribute("error", "会員IDは数字で入力してください");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
				rd.forward(request, response);
			}
			String bk0 = request.getParameter("bookId0");
			if (bk0.equals("")) {
			} else {
				check.formCheck(bk0);
			}
			if (check.getError() == null) {
			} else {
				request.setAttribute("error", "図書IDは数字で入力してください");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
				rd.forward(request, response);
			}
			RentalDao search = new RentalDao();
			List<RentalBean> list = new ArrayList<RentalBean>();
			if (useId.equals("") && bk0.equals("")) {
				list = search.searchAll();
			} else if (useId.equals("")) {
				int book0 = Integer.parseInt(bk0);
				list = search.searchBook(book0);
			} else if (bk0.equals("")) {
				int userId = Integer.parseInt(useId);
				list = search.searchUser(userId);
			} else {
				int book0 = Integer.parseInt(bk0);
				int userId = Integer.parseInt(useId);
				list = search.searchDouble(userId, book0);
			}
			request.setAttribute("data", list);
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalSearchResult.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/error.jsp");
			rd.forward(request, response);
		}
	}

}
