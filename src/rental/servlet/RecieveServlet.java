package rental.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookBean;
import rental.bean.CheckInputData;
import rental.dao.RentalDao;
import user.bean.UserBean;
import user.dao.UserDAO;

/**
 * 貸出作業のServlet
 *
 * @author Ryoki Morohashi
 */
@WebServlet("/RecieveServlet")
public class RecieveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RecieveServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		try {
			String action = "";
			action = request.getParameter("action");
			// 情報入力より「確認画面へ」を押下する
			if (action.equals("check")) {
				CheckInputData check = new CheckInputData();
				String useId = request.getParameter("userId");
				if (useId.equals("")) {
					request.setAttribute("error", "会員IDを記入してください");
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
					rd.forward(request, response);
				}
				check.formCheck(useId);
				if (check.getError() == null) {
				} else {
					request.setAttribute("error", "会員IDは数字で入力してください");
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
					rd.forward(request, response);
				}
				int userId = Integer.parseInt(useId);
				check.setCheckUserId(userId);
				String bk1 = request.getParameter("bookId1");
				String bk2 = request.getParameter("bookId2");
				String bk3 = request.getParameter("bookId3");
				String bk4 = request.getParameter("bookId4");
				String bk5 = request.getParameter("bookId5");
				if (bk1.equals("")) {
				} else {
					check.formCheck(bk1);
				}
				if (bk2.equals("")) {
				} else {
					check.formCheck(bk2);
				}
				if (bk3.equals("")) {
				} else {
					check.formCheck(bk3);
				}
				if (bk4.equals("")) {
				} else {
					check.formCheck(bk4);
				}
				if (bk5.equals("")) {
				} else {
					check.formCheck(bk5);
				}
				if (check.getError() == null) {
				} else {
					request.setAttribute("error", "図書IDは数字で入力してください");
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
					rd.forward(request, response);
				}
				if (bk1.equals("")) {
					bk1 = "-1";
				}
				if (bk2.equals("")) {
					bk2 = "-1";
				}
				if (bk3.equals("")) {
					bk3 = "-1";
				}
				if (bk4.equals("")) {
					bk4 = "-1";
				}
				if (bk5.equals("")) {
					bk5 = "-1";
				}
				int book1 = Integer.parseInt(bk1);
				int book2 = Integer.parseInt(bk2);
				int book3 = Integer.parseInt(bk3);
				int book4 = Integer.parseInt(bk4);
				int book5 = Integer.parseInt(bk5);
				if (book1 == -1 && book2 == -1 && book3 == -1 && book4 == -1 && book5 == -1) {
					request.setAttribute("error", "図書IDを1つ以上記入してください。");
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
					rd.forward(request, response);
				}
				HttpSession session = request.getSession();
				session.setAttribute("act", "rec");
				session.setAttribute("userId", userId);
				if (book1 != -1) {
					;
					check.setCheckBookId(book1);
					check.checkRecieveData();
				}
				if (book2 != -1) {
					check.setCheckBookId(book2);
					check.checkRecieveData();
				}
				if (book3 != -1) {
					check.setCheckBookId(book3);
					check.checkRecieveData();
				}
				if (book4 != -1) {
					check.setCheckBookId(book4);
					check.checkRecieveData();
				}
				if (book5 != -1) {
					check.setCheckBookId(book5);
					check.checkRecieveData();
				}
				if (check.getError() == null) {
				} else {
					request.setAttribute("error", check.getError());
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
					rd.forward(request, response);
				}
				UserDAO callU = new UserDAO();
				UserBean user = callU.selectByUser(userId);
				session.setAttribute("user", user);
				CheckInputData bk = new CheckInputData();
				List<BookBean> list = new ArrayList<BookBean>();
				if (book1 == -1) {
				} else {
					BookBean bok1 = bk.ReturnBookInfo(book1);
					list.add(bok1);
				}
				if (book2 == -1) {
				} else {
					BookBean bok2 = bk.ReturnBookInfo(book2);
					list.add(bok2);
				}
				if (book3 == -1) {
				} else {
					BookBean bok3 = bk.ReturnBookInfo(book3);
					list.add(bok3);
				}
				if (book4 == -1) {
				} else {
					BookBean bok4 = bk.ReturnBookInfo(book4);
					list.add(bok4);
				}
				if (book5 == -1) {
				} else {
					BookBean bok5 = bk.ReturnBookInfo(book5);
					list.add(bok5);
				}
				session.setAttribute("list", list);
				session.setAttribute("user", user);
				session.setAttribute("book1", book1);
				session.setAttribute("book2", book2);
				session.setAttribute("book3", book3);
				session.setAttribute("book4", book4);
				session.setAttribute("book5", book5);
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/CheckRentalInfo.jsp");
				rd.forward(request, response);
				// 情報確認から「返却」ボタンを押下する
			} else if (action.equals("finish")) {
				HttpSession session = request.getSession();
				RentalDao finish = new RentalDao();
				int i=0;
				int userid = (int) session.getAttribute("userId");
				int bookid1 = (int) session.getAttribute("book1");
				int bookid2 = (int) session.getAttribute("book2");
				int bookid3 = (int) session.getAttribute("book3");
				int bookid4 = (int) session.getAttribute("book4");
				int bookid5 = (int) session.getAttribute("book5");
				if (bookid1 == -1) {
				} else {
					i+=finish.recieveBook(userid, bookid1);
				}
				if (bookid2 == -1) {
				} else {
					i+=finish.recieveBook(userid, bookid2);
				}
				if (bookid3 == -1) {
				} else {
					i+=finish.recieveBook(userid, bookid3);
				}
				if (bookid4 == -1) {
				} else {
					i+=finish.recieveBook(userid, bookid4);
				}
				if (bookid5 == -1) {
				} else {
					i+=finish.recieveBook(userid, bookid5);
				}
				if (i==0)
				{
					request.setAttribute("error", "既にこの本は返却されています。");
					RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
					rd.forward(request, response);
				}
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/RecieveFinish.jsp");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/error.jsp");
			rd.forward(request, response);
		}
	}
}
