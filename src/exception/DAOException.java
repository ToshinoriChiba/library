/**
 * DAO系の例外
 * @author chibatoshinori
 */

package exception;

public class DAOException extends Exception {
	public DAOException(String message) {
		super(message);
	}
}

