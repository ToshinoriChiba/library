/**
 * 会員情報のBean
 * @author chibatoshinori
 */
package user.bean;

import java.io.Serializable;
import java.sql.Date;

public class UserBean implements Serializable {
	// フィールド
	/** 利用者ID */
	private int userId;

	/** 利用者姓 */
	private String familyName = "";

	/** 利用者名 */
	private String name = "";

	/** 郵便番号 */
	private String postal = "";

	/** 住所 */
	private String address = "";

	/** 電話番号 */
	private String tel = "";

	/** メールアドレス */
	private String email = "";

	/** 生年月日 */
	private Date birthday = null;

	/** 入会日 */
	private Date joinDay = null;

	/** 退会日 */
	private Date leaveDay = null;


	public UserBean(int userId, String familyName, String name, String postal, String address, String tel, String email, Date birthday, Date joinDay, Date leaveDay) {
		setUserId(userId);
		setFamilyName(familyName);
		setName(name);
		setPostal(postal);
		setAddress(address);
		setTel(tel);
		setEmail(email);
		setBirthday(birthday);
		setJoinDay(joinDay);
		setLeaveDay(leaveDay);
	}

	public UserBean(int userId, String familyName, String name, String postal, String address, String tel, String email, Date birthday, Date joinDay) {
		setUserId(userId);
		setFamilyName(familyName);
		setName(name);
		setPostal(postal);
		setAddress(address);
		setTel(tel);
		setEmail(email);
		setBirthday(birthday);
		setJoinDay(joinDay);
	}
	public UserBean(int userId, String familyName, String name, String postal, String address, String tel, String email, Date birthday) {
		setUserId(userId);
		setFamilyName(familyName);
		setName(name);
		setPostal(postal);
		setAddress(address);
		setTel(tel);
		setEmail(email);
		setBirthday(birthday);
	}

	public UserBean(String familyName, String name, String postal, String address, String tel, String email, Date birthday, Date joinDay, Date leaveDay) {
		setFamilyName(familyName);
		setName(name);
		setPostal(postal);
		setAddress(address);
		setTel(tel);
		setEmail(email);
		setBirthday(birthday);
		setJoinDay(joinDay);
		setLeaveDay(leaveDay);
	}
	public UserBean(String familyName, String name, String postal, String address, String tel, String email, Date birthday, Date joinDay) {
		setFamilyName(familyName);
		setName(name);
		setPostal(postal);
		setAddress(address);
		setTel(tel);
		setEmail(email);
		setBirthday(birthday);
		setJoinDay(joinDay);
	}
	public UserBean(String familyName, String name, String postal, String address, String tel, String email, Date birthday) {
		setFamilyName(familyName);
		setName(name);
		setPostal(postal);
		setAddress(address);
		setTel(tel);
		setEmail(email);
		setBirthday(birthday);
	}

	public UserBean( ) {

	}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Date getJoinDay() {
		return joinDay;
	}
	public void setJoinDay(Date joinDay) {
		this.joinDay = joinDay;
	}
	public Date getLeaveDay() {
		return leaveDay;
	}
	public void setLeaveDay(Date leaveDay) {
		this.leaveDay = leaveDay;
	}


}
