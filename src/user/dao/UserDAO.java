/**
 * 会員系のDBを操作するDAO
 * @author chibatoshinori
 */

package user.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 * @author chibatoshinori
 */
import java.util.List;

import book.dao.DBpass;
import exception.DAOException;
import user.bean.UserBean;

public class UserDAO {
	private Connection con;

	public UserDAO() throws DAOException {
		getConnection();
	}

	/**
	 * @author chibatoshinori
	 * @return List<Userbean> : DBから取得した会員情報のレコードを返す
	 * @throws DAOException
	 */
	public List<UserBean> selectAll() throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		List<UserBean> userList = null;
		try {
			// SQL文の作成
			// TODO 1回にセレクトする数を制限する。
			String sql = "SELECT user_id, user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day FROM userinf LIMIT 10";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得及び表示
			userList = new ArrayList<>();
			while (rs.next()) {
				int userId = rs.getInt("user_id");
				String familyName = rs.getString("user_family_name");
				String name = rs.getString("user_name");
				String postal = rs.getString("user_postal");
				String address = rs.getString("user_address");
				String tel = rs.getString("user_tel");
				String email = rs.getString("user_email");
				Date birthday = rs.getDate("user_birthday");
				Date joinDay = rs.getDate("join_day");
				Date leaveDay = rs.getDate("leave_day");

				UserBean bean = new UserBean(userId, familyName, name, postal, address, tel, email, birthday, joinDay, leaveDay);
				userList.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return userList;
	}

	/**
	 * emailで会員情報をDBから取得するメソッド
	 * @author chibatoshinori
	 * @param email
	 * @return
	 * @throws DAOException
	 */
	public UserBean selectByEmail(String email) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			// SQL文の作成
			String sql = "SELECT user_id, user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day FROM userinf WHERE user_email = ? and leave_day IS NULL";

			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// メールの指定
			st.setString(1, email);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得
			if (rs.next()) {
				int userId = rs.getInt("user_id");
				String familyName = rs.getString("user_family_name");
				String name = rs.getString("user_name");
				String postal = rs.getString("user_postal");
				String address = rs.getString("user_address");
				String tel = rs.getString("user_tel");
				String email1 = rs.getString("user_email");
				Date birthday = rs.getDate("user_birthday");
				Date joinDay = rs.getDate("join_day");
				Date leaveDay = rs.getDate("leave_day");

				UserBean bean = new UserBean(userId, familyName, name, postal, address, tel, email1, birthday, joinDay, leaveDay);
				return bean;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}



	public UserBean selectByUser(int userid) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			// SQL文の作成
			String sql = "SELECT user_id, user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day FROM userinf where user_id = ? AND leave_day is NULL";

			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// メールの指定
			st.setInt(1, userid);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得
			if (rs.next()) {
				int userId = rs.getInt("user_id");
				String familyName = rs.getString("user_family_name");
				String name = rs.getString("user_name");
				String postal = rs.getString("user_postal");
				String address = rs.getString("user_address");
				String tel = rs.getString("user_tel");
				String email1 = rs.getString("user_email");
				Date birthday = rs.getDate("user_birthday");
				Date joinDay = rs.getDate("join_day");
				Date leaveDay = rs.getDate("leave_day");

				UserBean bean = new UserBean(userId, familyName, name, postal, address, tel, email1, birthday, joinDay, leaveDay);
				return bean;
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}

	/**
	 * DBに会員データを挿入するメソッド
	 * 加入日が指定されている場合はその日を
	 * 加入日が指定されていない場合はCURRENT_DATEをjoin_dayに入れる
	 * @author chibatoshinori
	 * @param user
	 * @return
	 * @throws DAOException
	 */
	public boolean insert(UserBean user) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		try {
			// SQL文の作成
			String sql;
			if (user.getJoinDay() == null) {
				sql = "INSERT INTO userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address,join_day) VALUES(?, ?, ?, ?, ?, ?, ?,current_date)";
			} else {
				sql = "INSERT INTO userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
			}
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 利用者情報の指定
			st.setString(1, user.getFamilyName());
			st.setString(2, user.getName());
			st.setDate(3, user.getBirthday());
			st.setString(4, user.getTel());
			st.setString(5, user.getEmail());
			st.setString(6, user.getPostal());
			st.setString(7, user.getAddress());
			if (user.getJoinDay() == null){}
			else{st.setDate(8, user.getJoinDay());	// 入会日の記述がない場合はCURRENT_DATEが代入される
			}
			// SQLの実行
			int rows = st.executeUpdate();

			if (rows > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました");
		} finally {
			try {
				// リソースの解放
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました");
			}
		}
	}


	public int update(UserBean user, int id) throws DAOException {
		if (con == null) getConnection();
		PreparedStatement st = null;
		// SQL文
		String sql = "";
		int rows = 0;

		try {
			// user_family_nameの変更
			if (user.getFamilyName() == null || user.getFamilyName().equals("")) {}
			else{
				sql = "UPDATE userinf SET user_family_name = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, user.getFamilyName());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// user_nameの変更
			if (user.getName() == null || user.getName().equals("")){}
				else {
				sql = "UPDATE userinf SET user_name = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, user.getName());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// user_telの変更
			if (user.getTel() == null || user.getTel().equals("")){}
				else
			{
				sql = "UPDATE userinf SET user_tel = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, user.getTel());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// user_emailの変更
			if (user.getEmail() == null || user.getEmail().equals("")){}
			else{
				sql = "UPDATE userinf SET user_email = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, user.getEmail());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// user_postalの変更
			if (user.getPostal() == null || user.getPostal().equals("")){}
			else{
				sql = "UPDATE userinf SET user_postal = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, user.getPostal());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// user_addressの変更
			if(user.getAddress()==null||user.getAddress().equals("")){}
			else {
				sql = "UPDATE userinf SET user_address = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, user.getAddress());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// user_birthdayの変更
			if (user.getBirthday() != null) {
				sql = "UPDATE userinf SET user_birthday = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setDate(1, user.getBirthday());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// join_dayの変更
			if (user.getJoinDay() != null) {
				sql = "UPDATE userinf SET join_day = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setDate(1, user.getJoinDay());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// leave_dayの変更
			if (user.getLeaveDay() != null) {
				sql = "UPDATE userinf SET leave_day = ? WHERE user_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setDate(1, user.getLeaveDay());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}
			return rows;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました");
		} finally {
			try {
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました");
			}
		}

	}

	/**
	 * @author chibatoshinori
	 * DBに接続するためのメソッド
	 * @throws DAOException
	 */
	private void getConnection() throws DAOException {
		try {
			// JDBCドライバの登録
			Class.forName("org.postgresql.Driver");
			// URL, ユーザー名, パスワードの設定
			String url = "jdbc:postgresql:library";
			String user = "postgres";
			String password = (new DBpass()).getDBpass();
			// データベースへの接続
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw new DAOException("接続に失敗しました。");
		}
	}

	/**
	 * @author chibatoshinori
	 * DBへの接続を閉じるメソッド
	 * @throws DAOException
	 * @throws SQLException
	 */
	private void close() throws DAOException, SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}



	public int selectByUserReturnLeave(int userid) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			// SQL文の作成
			String sql = "SELECT user_id FROM userinf where user_id = ? and leave_day is null";

			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// メールの指定
			st.setInt(1, userid);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得
			if (rs.next()) {
				return 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}


}
