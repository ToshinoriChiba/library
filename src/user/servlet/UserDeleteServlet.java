package user.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import exception.DAOException;
import user.bean.UserBean;
import user.dao.UserDAO;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDeleteServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//response.getWriter().append("Served at: ").append(request.getContextPath());

		request.setCharacterEncoding("UTF-8");
		String email = request.getParameter("email");




		try {
			HttpSession session = request.getSession();
			UserDAO  sdao = new UserDAO();
			UserBean bean = sdao.selectByEmail(email);
			if(bean==null){
				request.setAttribute("error","emailはデータの存在するものを入力してください");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/usererror.jsp");
				rd.forward(request, response);
			}
			else{}

			request.setAttribute("bean",bean);
			session.setAttribute("userId",bean.getUserId() );
		} catch (DAOException e) {
			e.printStackTrace();
		}
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/userDeleteSearchResultView.jsp");
		rd.forward(request, response);





	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
