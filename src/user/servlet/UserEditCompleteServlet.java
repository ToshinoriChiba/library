package user.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import exception.DAOException;
import user.bean.UserBean;
import user.dao.UserDAO;

/**
 * Servlet implementation class UserEditCompleteServlet2
 */
@WebServlet("/UserEditCompleteServlet")
public class UserEditCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEditCompleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		UserBean user = (UserBean) session.getAttribute("user");
		UserBean bean = (UserBean) session.getAttribute("bean");

		session.removeAttribute("user");
		session.removeAttribute("bean");

		try {
			UserDAO dao = new UserDAO();
			dao.update(user, bean.getUserId());
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
			return;
		}
		gotoPage(request, response, "/WEB-INF/userEditComleteView.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
