package user.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import exception.DAOException;
import user.bean.UserBean;
import user.dao.UserDAO;

/**
 * Servlet implementation class UserSearchServlet
 */
@WebServlet("/UserSearchServlet")
public class UserSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSearchServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		request.setCharacterEncoding("UTF-8");
		String email = request.getParameter("email");

		try {
			UserDAO  sdao = new UserDAO();
			UserBean bean = sdao.selectByEmail(email);
			if (bean == null) {
				String error = "入力された会員メールアドレスに対応する会員情報が存在しません。正しく入力するか新規登録してください";
				request.setAttribute("error", error);
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/usererror.jsp");
				rd.forward(request, response);
				return;
			} else {
			request.setAttribute("bean",bean);

			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/UserSearchResultView.jsp");
			rd.forward(request, response);
			}
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}


}
