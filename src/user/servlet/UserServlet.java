/**
 * 会員系の処理を振り分けるメソッド
 * @author chibatoshinori
 */

package user.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// パラメータの解析
		String action = request.getParameter("action");
		// 遷移先を入れる変数
		String page = "";
		// actionの値により各機能へ振り分け
		switch (action) {
		case "search" : page = "/UserSearchServlet"; break;
		case "add" : page = "/UserAddServlet"; break;
		case "edit" : page = "/UserEditServlet"; break;
		case "delete" : page = "/UserDeleteServlet"; break;
		default : page = "/UserSearchServlet"; break;
		}
		gotoPage(request, response, page);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
