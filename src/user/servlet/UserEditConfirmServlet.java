package user.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import user.bean.UserBean;



/**
 * Servlet implementation class UserEditConfirmServlet
 */
@WebServlet("/UserEditConfirmServlet")
public class UserEditConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEditConfirmServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String user_family_name= request.getParameter("familyName");
		String user_name= request.getParameter("name");
		String userBirthday = request.getParameter("birthday");
		Date birthday = null;
		// Date型への変換
		if (!userBirthday.equals("")) {
			birthday = Date.valueOf(userBirthday);
			Date today = new Date(System.currentTimeMillis());
			if (birthday.after(today)) {
				String error = "生年月日の値が不正です。本日より前の日付けを入力してください。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
		}
		String user_tel = request.getParameter("tel");
		String user_email = request.getParameter("email");
		String user_postal = request.getParameter("postal");
		String user_address = request.getParameter("address");
		UserBean user = new UserBean(user_family_name, user_name,user_postal,user_address,user_tel,user_email ,birthday);

		HttpSession session = request.getSession();
		session.setAttribute("user", user);

		gotoPage(request, response, "/WEB-INF/UserEditConfirmView.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
