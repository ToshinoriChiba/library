/**
 * 会員追加を完了するServlet
 * @author chibatoshinori
 */

package user.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import exception.DAOException;
import user.bean.UserBean;
import user.dao.UserDAO;

/**
 * Servlet implementation class UserAddCompleteServlet
 */
@WebServlet("/UserAddCompleteServlet")
public class UserAddCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddCompleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBean user = (UserBean) session.getAttribute("user");

		try {
			UserDAO dao = new UserDAO();
			UserBean confirm = dao.selectByEmail(user.getEmail());
			if (confirm != null && !confirm.getEmail().equals("")) {
				String error = "既に同じemailアドレスの会員が登録済みです。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
			dao.insert(user);
			gotoPage(request, response, "/WEB-INF/userAddCompleteView.jsp");
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
