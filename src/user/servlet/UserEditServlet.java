package user.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import exception.DAOException;
import user.bean.UserBean;
import user.dao.UserDAO;

/**
 * Servlet implementation class UserEdit
 */
@WebServlet("/UserEditServlet")
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserEditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String email = request.getParameter("email");

		try {

			UserDAO  dao = new UserDAO();
			UserBean bean = dao.selectByEmail(email);
			if (bean == null) {
				String error = "入力された会員メールアドレスに対応する会員情報が存在しません。正しく入力しますか新規登録してください";
				request.setAttribute("error", error);
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/rentalerror.jsp");
				rd.forward(request, response);
				return;
			} else {

			HttpSession session = request.getSession();
			session.setAttribute("bean",bean);

			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/userEditSelectView.jsp");
			rd.forward(request, response);
			}
		} catch (DAOException e) {
			e.printStackTrace();
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
