/**
 * 会員追加を行うServlet
 * @author chibatoshinori
 */
package user.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import user.bean.UserBean;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserAddServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String familyName = request.getParameter("familyName");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");
		String tel = request.getParameter("tel");
		String email = request.getParameter("email");
		String postal = request.getParameter("postal");
		String address = request.getParameter("address");
		String join = request.getParameter("arrival");
		Date birthDay = null;
		Date joinDay = null;

		// Date型への変換
		if (!birthday.equals("")) {
			birthDay = Date.valueOf(birthday);
			Date today = new Date(System.currentTimeMillis());
			if (birthDay.after(today)) {
				String error = "生年月日の値が不正です。本日より前の日付けを入力してください。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
		}

		if (!join.equals("")) {
			joinDay = Date.valueOf(join);
		}
		UserBean user = new UserBean(familyName, name, postal, address, tel, email, birthDay, joinDay);
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		gotoPage(request, response, "/WEB-INF/userAddConfirmView.jsp");


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
