/**
 * book_infoテーブルを操作するDAO
 * @author chibatoshinori
 */

package book.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import book.bean.BookInfoBean;
import exception.DAOException;

public class BookInfoDAO {
	private Connection con;

	public BookInfoDAO() throws DAOException {
		getConnection();
	}

	/**
	 * 図書情報のテーブルから全件取得するメソッド(とりあえず10件に制限している)
	 * @return List<BookInfoBean>
	 * @throws DAOException
	 * @author chibatoshinori
	 */
	public List<BookInfoBean> selectBookInfoAll() throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		List<BookInfoBean> bookList = null;
		try {
			// SQL文の作成
			// TODO 1回にセレクトする数を制限する。
			/**
			 * このSQL文の作成部分は修正の必要あり
			 */
			String sql = "SELECT book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day FROM book_info LIMIT 10";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得及び表示
			bookList = new ArrayList<>();
			while (rs.next()) {
				String isbn = rs.getString("book_info_isbn");
				int categoryCode = rs.getInt("category_code");
				int publisherCode = rs.getInt("publisher_code");
				String bookName = rs.getString("book_info_name");
				String author = rs.getString("book_info_author");
				Date publicationDay = rs.getDate("publication_day");

				BookInfoBean bean = new BookInfoBean(isbn, categoryCode, publisherCode, bookName, author, publicationDay);
				bookList.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return bookList;
	}

	/**
	 * ISBN番号から対応する値を取得するメソッド
	 * @param isbn
	 * @return
	 * @throws DAOException
	 */
	public BookInfoBean selectByISBN(String isbn) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			// SQL文の作成
			String sql = "SELECT book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day FROM book_info where book_info_isbn = ?";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// ISBNの指定
			st.setString(1, isbn);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得及び表示

			if (rs.next()) {
				String isb = rs.getString("book_info_isbn");
				int categoryCode = rs.getInt("category_code");
				int publisherCode = rs.getInt("publisher_code");
				String bookName = rs.getString("book_info_name");
				String author = rs.getString("book_info_author");
				Date publicationDay = rs.getDate("publication_day");

				BookInfoBean bean = new BookInfoBean(isb, categoryCode, publisherCode, bookName, author, publicationDay);
				return bean;
			} else {
				// 該当するレコードなし
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}

	/**
	 * book_stateテーブルにデータを挿入するメソッド
	 * @param book
	 * @return int
	 * @throws DAOException
	 * @author chibatoshinori
	 */
	public int insertBookInfo(BookInfoBean bookInfo) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		try {
			// SQL文の作成
			String sql = "INSERT INTO book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) VALUES(?, ?, ?, ?, ?, ?)";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 図書情報情報の指定
			st.setString(1, bookInfo.getIsbn());
			st.setInt(2, bookInfo.getCategoryCode());
			st.setInt(3, bookInfo.getPublisherCode());
			st.setString(4, bookInfo.getBookName());
			st.setString(5, bookInfo.getAuthor());
			st.setDate(6, bookInfo.getPublicationDay());

			// SQLの実行
			int rows = st.executeUpdate();

			return rows;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました");
		} finally {
			try {
				// リソースの解放
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました");
			}
		}
	}

	/**
	 * 図書情報の更新
	 * @param book
	 * @return int
	 * @throws DAOException
	 */
	public int updateBookInfo(BookInfoBean book, String isbn) throws DAOException {
		if (con == null) getConnection();
		PreparedStatement st = null;
		PreparedStatement st1 = null;
		// SQL文
		String sql = "";
		int rows = 0;

		try {

			if (con == null) getConnection();
			// 分類番号の変更
			if (book.getCategoryCode() != -1) {
				sql = "UPDATE book_info SET category_code = ? WHERE book_info_isbn = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setInt(1, book.getCategoryCode());
				st.setString(2, isbn);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// 出版社番号の変更
			if (book.getPublisherCode() != -1) {
				sql = "UPDATE book_info SET publisher_code = ? WHERE book_info_isbn = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setInt(1, book.getPublisherCode());
				st.setString(2, isbn);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// 書名の変更
			if (book.getBookName() != "") {
				sql = "UPDATE book_state SET book_info_name = ? WHERE book_info_isbn = ?";
				st1 = con.prepareStatement(sql);
				st1.setString(1, book.getBookName());
				st1.setString(2, isbn);
				rows += st1.executeUpdate();
				st1.close();
				sql = "UPDATE book_info SET book_info_name = ? WHERE book_info_isbn = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, book.getBookName());
				st.setString(2, isbn);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// 著者の変更
			if (book.getAuthor() != "") {
				sql = "UPDATE book_info SET book_info_author = ? WHERE book_info_isbn = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, book.getAuthor());
				st.setString(2, isbn);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// 出版日の変更
			if (book.getPublicationDay() != null) {
				sql = "UPDATE book_info SET publication_day = ? WHERE book_info_isbn = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setDate(1, book.getPublicationDay());
				st.setString(2, isbn);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// isbnの変更
			if (book.getIsbn() != "") {
				updateISBN(isbn, book.getIsbn());
			}
			return rows;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました");
		} finally {
			try {
				if (st != null) st.close();
				if (st1 != null) st1.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました");
			}
		}
	}

	/**
	 * @author chibatoshinori
	 * DBに接続するためのメソッド
	 * @throws DAOException
	 */
	private void getConnection() throws DAOException {
		try {
			// JDBCドライバの登録
			Class.forName("org.postgresql.Driver");
			// URL, ユーザー名, パスワードの設定
			String url = "jdbc:postgresql:library";
			String user = "postgres";
			String password = (new DBpass()).getDBpass();
			// データベースへの接続
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw new DAOException("接続に失敗しました。");
		}
	}

	/**
	 * @author chibatoshinori
	 * DBへの接続を閉じるメソッド
	 * @throws DAOException
	 * @throws SQLException
	 */
	private void close() throws DAOException, SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

	@SuppressWarnings("resource")
	public void updateISBN(String isbn, String isbn_after) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		PreparedStatement st1 = null;
		PreparedStatement st2 = null;
		PreparedStatement st3 = null;
		ResultSet rs = null;
		String tmp = "0000000000000";
		// 変更用のdataがbook_infoにあるか確認
		String sql = "SELECT book_info FROM book_info WHERE book_info_isbn = ?";
		try {
			st = con.prepareStatement(sql);
			st.setString(1, tmp);
			rs = st.executeQuery();

			if (!rs.next()) {
				BookInfoDAO dao = new BookInfoDAO();
				@SuppressWarnings("deprecation")
				Date date = new Date(1900, 1, 1);
				BookInfoBean bookInfo = new BookInfoBean(tmp , 1, 1, "データ更新時回避用データ", "author", date);
				dao.insertBookInfo(bookInfo);
			}

			// ISBNを更新
			sql = "UPDATE book_state SET book_info_isbn = ? WHERE book_info_isbn = ?";
			st1 = con.prepareStatement(sql);
			st1.setString(1, tmp);
			st1.setString(2, isbn);
			st1.executeUpdate();

			// BookInfoのISBNを更新
			sql = "UPDATE book_info SET book_info_isbn = ? WHERE book_info_isbn = ?";
			st2 = con.prepareStatement(sql);
			st2.setString(1, isbn_after);
			st2.setString(2, isbn);
			st2.executeUpdate();

			// BookStateのISBNを戻す
			sql = "UPDATE book_state SET book_info_isbn = ? WHERE book_info_isbn = ?";
			st3 = con.prepareStatement(sql);
			st3.setString(1, isbn_after);
			st3.setString(2, tmp);
			st3.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st3 != null) st3.close();
				if (st2 != null) st2.close();
				if (st1 != null) st1.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}



}
