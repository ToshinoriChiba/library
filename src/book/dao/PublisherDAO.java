/**
 * publisherテーブルを操作するDAO
 * @author chibatoshinori
 */

package book.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import book.bean.PublisherBean;
import exception.DAOException;

public class PublisherDAO {
	private Connection con;

	public PublisherDAO() throws DAOException {
		getConnection();
	}

	/**
	 * 出版社情報を全件取得する
	 * @return List<PublisherBean>
	 * @throws DAOException
	 */
	public List<PublisherBean> selectPublisherAll() throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		List<PublisherBean> publisherList = null;
		try {

			String sql = "SELECT publisher_code, publisher_name from publisher";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得及び表示
			publisherList = new ArrayList<>();
			while (rs.next()) {
				int publisherCode = rs.getInt("publisher_code");
				String publisherName = rs.getString("publisher_name");

				PublisherBean bean = new PublisherBean(publisherCode, publisherName);
				publisherList.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return publisherList;
	}

	/**
	 * 出版社番号から出版社名を取得する
	 * @param publisherCode
	 * @return String
	 * @throws DAOException
	 */
	public String selectByPublisherCode(int publisherCode) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		String publisherName = "";

		try {
			// SQL文の作成
			String sql = "SELECT publisher_name from publisher where publisher_code = ?";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 分類番号の指定
			st.setInt(1, publisherCode);
			// SQLの実行
			rs = st.executeQuery();

			if (rs.next()) {
				publisherName = rs.getString("publisher_name");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return publisherName;
	}

	/**
	 * 出版社名から出版社番号を取得する
	 * @param publisherName
	 * @return int
	 * @throws DAOException
	 */
	public int selectByPublisherName(String publisherName) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		int publisherCode = 0;

		try {
			// SQL文の作成
			String sql = "SELECT publisher_name from publisher where publisher_name = ?";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 分類名の指定
			st.setString(1, publisherName);
			// SQLの実行
			rs = st.executeQuery();
			if (rs.next()) {
				publisherCode = rs.getInt("publisher_code");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return publisherCode;
	}

	/**
	 * @author chibatoshinori
	 * DBに接続するためのメソッド
	 * @throws DAOException
	 */
	private void getConnection() throws DAOException {
		try {
			// JDBCドライバの登録
			Class.forName("org.postgresql.Driver");
			// URL, ユーザー名, パスワードの設定
			String url = "jdbc:postgresql:library";
			String user = "postgres";
			String password = "himitu";
			// データベースへの接続
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw new DAOException("接続に失敗しました。");
		}
	}

	/**
	 * @author chibatoshinori
	 * DBへの接続を閉じるメソッド
	 * @throws DAOException
	 * @throws SQLException
	 */
	private void close() throws DAOException, SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

}
