/**
 * DBのテーブルBookStateを操作するDAO
 * @author chibatoshinori
 */

package book.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import book.bean.BookStateBean;
import exception.DAOException;

public class BookStateDAO {
	private Connection con;

	public BookStateDAO() throws DAOException {
		getConnection();
	}

	/**
	 * 全件取得するメソッド
	 * 取得数の制限, 取得する範囲を、まだ設定できていないので修正必要あり
	 * @return List<BookStatebean>
	 * @throws DAOException
	 * @author chibatoshinori
	 */
	public List<BookStateBean> selectBookStateAll() throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		List<BookStateBean> bookList = null;
		try {
			// SQL文の作成
			// TODO 1回にセレクトする数を制限する。
			/**
			 * このSQL文の作成部分は修正の必要あり
			 */
			String sql = "SELECT book_state_id, book_info_isbn, book_info_name, arrival_date, disposal_date, remark FROM book_state LIMIT 10";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得及び表示
			bookList = new ArrayList<>();
			while (rs.next()) {
				int bookId = rs.getInt("book_state_id");
				String isbn = rs.getString("book_info_isbn");
				String bookName = rs.getString("book_info_name");
				Date arrivalDate  = rs.getDate("arrival_date");
				Date disposalDate = rs.getDate("disposal_date");
				String remark = rs.getString("remark");

				BookStateBean bean = new BookStateBean(bookId, isbn, bookName, arrivalDate, disposalDate, remark);
				bookList.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return bookList;
	}

	/**
	 * 図書IDでDBから図書データを取得するメソッド
	 * @param id
	 * @return List<BookStateBean>
	 * @throws DAOException
	 * @author chibatoshinori
	 */
	public BookStateBean selectByBookId(int id) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			// SQL文の作成
			String sql = "SELECT book_state_id, book_info_isbn, book_info_name, arrival_date, disposal_date, remark FROM book_state WHERE book_state_id = ?";

			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 図書IDの指定
			st.setInt(1, id);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得
			if (rs.next()) {
				int bookId = rs.getInt("book_state_id");
				String isbn = rs.getString("book_info_isbn");
				String bookName = rs.getString("book_info_name");
				Date arrivalDate  = rs.getDate("arrival_date");
				Date disposalDate = rs.getDate("disposal_date");
				String remark = rs.getString("remark");

				BookStateBean bean = new BookStateBean(bookId, isbn, bookName, arrivalDate, disposalDate, remark);
				return bean;
			} else {
				// 回答レコードなし
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}
	/**
	 * book_infoのisbnが外部キー制約になっているために更新できないので回避するために利用するメソッド
	 * @param isbn
	 * @return
	 * @throws DAOException
	 */
	public List<BookStateBean> selectByISBN(String isbn) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		List<BookStateBean> list = new ArrayList<>();

		try {
			// SQL文の作成
			String sql = "SELECT book_state_id, book_info_isbn, book_info_name, arrival_date, disposal_date, remark FROM book_state WHERE book_info_isbn = ? WHERE disposal_date IS NULL";

			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 図書IDの指定
			st.setString(1, isbn);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得
			while (rs.next()) {
				int bookId = rs.getInt("book_state_id");
				String isbn1 = rs.getString("book_info_isbn");
				String bookName = rs.getString("book_info_name");
				Date arrivalDate  = rs.getDate("arrival_date");
				Date disposalDate = rs.getDate("disposal_date");
				String remark = rs.getString("remark");

				BookStateBean bean = new BookStateBean(bookId, isbn1, bookName, arrivalDate, disposalDate, remark);
				list.add(bean);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}

	/**
	 * book_stateテーブルにデータを挿入するメソッド
	 * @param book
	 * @return int
	 * @throws DAOException
	 * @author chibatoshinori
	 */
	public int insertBookState(BookStateBean book) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		try {
			// SQL文の作成
			String sql = "INSERT INTO book_state(book_info_isbn, book_info_name, arrival_date, disposal_date, remark) VALUES(?, ?, ?, ?, ?)";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 図書情報情報の指定
			st.setString(1, book.getIsbn());
			st.setString(2, book.getBookName());
			st.setDate(3, book.getArrivalDate());
			st.setDate(4, book.getDisposalDate());
			st.setString(5, book.getRemark());

			// SQLの実行
			int rows = st.executeUpdate();

			return rows;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました");
		} finally {
			try {
				// リソースの解放
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました");
			}
		}
	}

	/**
	 * 入荷日付, 廃棄日付, 備考の編集
	 * それ以外はbook_infoの情報なので触らない
	 * @param book
	 * @return int
	 * @throws DAOException
	 */
	public int updateBookState(BookStateBean book, int id) throws DAOException {
		if (con == null) getConnection();
		PreparedStatement st = null;
		// SQL文
		String sql = "";
		int rows = 0;

		try {
			// arrivalDateの変更
			if (book.getArrivalDate() != null) {
				sql = "UPDATE book_state SET arrival_date = ? WHERE user_state_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setDate(1, book.getArrivalDate());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// disposalDateの変更
			if (book.getDisposalDate() != null) {
				sql = "UPDATE book_state SET disposal_date = ? WHERE book_state_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setDate(1, book.getDisposalDate());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}

			// remarkの変更
			if (book.getRemark() != "") {
				sql = "UPDATE book_state SET remark = ? WHERE book_state_id = ?";
				// PreparedStatementオブジェクトの取得
				st = con.prepareStatement(sql);
				st.setString(1, book.getRemark());
				st.setInt(2, id);
				// SQLの実行
				rows += st.executeUpdate();
				st.close();
			}
			return rows;

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました");
		} finally {
			try {
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました");
			}
		}
	}
	/**
	 *
	 * @param isbn			// 変更前のISBN
	 * @param isbn_after	// 変更後のISBN
	 * @throws DAOException
	 */

	public int deleteBookState(int id) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		try {
			// SQL文の作成
			String sql = "DELETE FROM book_state WHERE book_state_id = ?";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 図書IDの指定
			st.setInt(1, id);
			// SQLの実行
			int rows = st.executeUpdate();
			return rows;
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの操作に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}

	/**
	 * @author chibatoshinori
	 * DBに接続するためのメソッド
	 * @throws DAOException
	 */
	private void getConnection() throws DAOException {
		try {
			// JDBCドライバの登録
			Class.forName("org.postgresql.Driver");
			// URL, ユーザー名, パスワードの設定
			String url = "jdbc:postgresql:library";
			String user = "postgres";
			String password = (new DBpass()).getDBpass();			// パスワードが大丈夫か不明 5/23
			// データベースへの接続
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw new DAOException("接続に失敗しました。");
		}
	}

	/**
	 * @author chibatoshinori
	 * DBへの接続を閉じるメソッド
	 * @throws DAOException
	 * @throws SQLException
	 */
	private void close() throws DAOException, SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

	public int selectByBookIdReturnSize(int id) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			// SQL文の作成
			String sql = "SELECT book_state_id FROM book_state WHERE book_state_id = ?";

			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 図書IDの指定
			st.setInt(1, id);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得
			if (rs.next()) {
				return 1;
			} else {
				// 回答レコードなし
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
	}

}
