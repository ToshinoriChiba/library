/**
 * categoryテーブルを操作するDAO
 * メソッドはselect系のみ
 * @author chibatoshinori
 */

package book.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import book.bean.CategoryBean;
import exception.DAOException;

public class CategoryDAO {
	private Connection con;

	public CategoryDAO() throws DAOException {
		getConnection();
	}

	public List<CategoryBean> selectCategoryAll() throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;
		List<CategoryBean> categoryList = null;
		try {

			String sql = "SELECT category_code, category_name from category";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// SQLの実行
			rs = st.executeQuery();
			// 結果の取得及び表示
			categoryList = new ArrayList<>();
			while (rs.next()) {
				int categoryCode = rs.getInt("category_code");
				String categoryName = rs.getString("category_name");

				CategoryBean bean = new CategoryBean(categoryCode, categoryName);
				categoryList.add(bean);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return categoryList;
	}

	/**
	 * 分類番号から分類名を取得する
	 * @param categoryCode
	 * @return String
	 * @throws DAOException
	 */
	public String selectByCategoryCode(int categoryCode) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		String categoryName = "";

		try {
			// SQL文の作成
			String sql = "SELECT category_name from category where category_code = ?";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 分類番号の指定
			st.setInt(1, categoryCode);
			// SQLの実行
			rs = st.executeQuery();

			if(rs.next()) {
				categoryName = rs.getString("category_name");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return categoryName;
	}

	/**
	 * 分類名から分類番号を取得する
	 * @param categoryName
	 * @return int
	 * @throws DAOException
	 */
	public int selectByCategoryName(String categoryName) throws DAOException {
		if (con == null) getConnection();

		PreparedStatement st = null;
		ResultSet rs = null;

		int categoryCode = 0;

		try {
			// SQL文の作成
			String sql = "SELECT category_name from category where category_name = ?";
			// PreparedStatementオブジェクトの取得
			st = con.prepareStatement(sql);
			// 分類名の指定
			st.setString(1, categoryName);
			// SQLの実行
			rs = st.executeQuery();
			if (rs.next()) {
				categoryCode = rs.getInt("category_code");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new DAOException("レコードの取得に失敗しました。");
		} finally {
			try {
				// リソースの解放
				if (rs != null) rs.close();
				if (st != null) st.close();
				close();
			} catch (Exception e) {
				throw new DAOException("リソースの解放に失敗しました。");
			}
		}
		return categoryCode;
	}

	/**
	 * @author chibatoshinori
	 * DBに接続するためのメソッド
	 * @throws DAOException
	 */
	private void getConnection() throws DAOException {
		try {
			// JDBCドライバの登録
			Class.forName("org.postgresql.Driver");
			// URL, ユーザー名, パスワードの設定
			String url = "jdbc:postgresql:library";
			String user = "postgres";
			String password = "himitu";
			// データベースへの接続
			con = DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			throw new DAOException("接続に失敗しました。");
		}
	}

	/**
	 * @author chibatoshinori
	 * DBへの接続を閉じるメソッド
	 * @throws DAOException
	 * @throws SQLException
	 */
	private void close() throws DAOException, SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

}
