/**
 * @author chibatoshinori
 */

package book.bean;

import java.io.Serializable;
import java.sql.Date;

public class BookStateBean implements Serializable {
	private int bookId;
	private String isbn = "";
	private String bookName = "";
	private Date arrivalDate = null;
	private Date disposalDate = null;
	private String remark = "";

	public BookStateBean() {

	}

	public BookStateBean(int bookId, String isbn, String bookName, Date arrivalDate, Date disposalDate, String remark) {
		setBookId(bookId);
		setIsbn(isbn);
		setBookName(bookName);
		setArrivalDate(arrivalDate);
		setDisposalDate(disposalDate);
		setRemark(remark);
	}

	public BookStateBean(int bookId, String isbn, String bookName, Date arrivalDate, String remark) {
		setBookId(bookId);
		setIsbn(isbn);
		setBookName(bookName);
		setArrivalDate(arrivalDate);
		setRemark(remark);
	}

	public BookStateBean(int bookId, String isbn, String bookName, Date arrivalDate) {
		setBookId(bookId);
		setIsbn(isbn);
		setBookName(bookName);
		setArrivalDate(arrivalDate);
	}

	public BookStateBean(int bookId, String isbn, String bookName, String remark) {
		setBookId(bookId);
		setIsbn(isbn);
		setBookName(bookName);
		setRemark(remark);
	}

	public BookStateBean(int bookId, String isbn, String bookName) {
		setBookId(bookId);
		setIsbn(isbn);
		setBookName(bookName);
	}
	public BookStateBean(String isbn, String bookName, Date arrivalDate) {
		setIsbn(isbn);
		setBookName(bookName);
		setArrivalDate(arrivalDate);
	}



	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getDisposalDate() {
		return disposalDate;
	}
	public void setDisposalDate(Date disposalDate) {
		this.disposalDate = disposalDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
