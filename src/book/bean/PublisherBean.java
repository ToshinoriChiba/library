/**
 * 出版社系Bean
 * @author chibatoshinori
 */

package book.bean;

import java.io.Serializable;

public class PublisherBean implements Serializable {
	/** 出版社コード */
	private int publisherCode = -1;

	/** 出版社名 */
	private String publisherName = "";

	public PublisherBean(int publisherCode, String publisherName) {
		setPublisherCode(publisherCode);
		setPublisherName(publisherName);
	}
	public PublisherBean(int publisherCode) {
		setPublisherCode(publisherCode);
	}
	public PublisherBean(String publisherName) {
		setPublisherName(publisherName);
	}


	public int getPublisherCode() {
		return publisherCode;
	}
	public void setPublisherCode(int publisherCode) {
		this.publisherCode = publisherCode;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

}
