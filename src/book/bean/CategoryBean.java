/**
 * 分離系Bean
 * @author chibatoshinori
 */

package book.bean;

import java.io.Serializable;

public class CategoryBean implements Serializable {
	/** 図書の分類コード */
	private int categoryCode;

	/** 図書の分類名 */
	private String categoryName;

	public CategoryBean(int categoryCode, String categoryName) {
		setCategoryCode(categoryCode);
		setCategoryName(categoryName);
	}
	public CategoryBean(int categoryCode) {
		setCategoryCode(categoryCode);
	}
	public CategoryBean(String categoryName) {
		setCategoryName(categoryName);
	}

	public int getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}



}
