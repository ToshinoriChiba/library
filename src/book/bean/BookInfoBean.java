/**
/**
 *
 */
package book.bean;

import java.io.Serializable;
import java.sql.Date;

import book.dao.CategoryDAO;
import book.dao.PublisherDAO;
import exception.DAOException;

public class BookInfoBean implements Serializable {
	/** 本のiSBN番号 */
	private String isbn = "";

	/** 本の分類番号 */
	private int categoryCode = -1;

	/** 出版社コード */
	private int publisherCode = -1;

	/** 本の名前 */
	private String bookName = "";

	/** 著者 */
	private String author = "";

	/** 出版日 */
	private Date publicationDay = null;

	public BookInfoBean() {

	}

	public BookInfoBean(String isbn, int categoryCode, int publisherCode, String bookName, String author, Date publicationDay) {
		setIsbn(isbn);
		setCategoryCode(categoryCode);
		setPublisherCode(publisherCode);
		setBookName(bookName);
		setAuthor(author);
		setPublicationDay(publicationDay);
	}

	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}
	public int getPublisherCode() {
		return publisherCode;
	}
	public void setPublisherCode(int publisherCode) {
		this.publisherCode = publisherCode;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Date getPublicationDay() {
		return publicationDay;
	}
	public void setPublicationDay(Date publicationDay) {
		this.publicationDay = publicationDay;
	}

	public String getCategoryName() {
		String name = "";
		try {
			CategoryDAO dao = new CategoryDAO();
			name = dao.selectByCategoryCode(this.categoryCode);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return name;
	}

	public String getPublisherName() {
		String name="";
		try {
			PublisherDAO dao = new PublisherDAO();
			name = dao.selectByPublisherCode(this.publisherCode);
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return name;
	}

}
