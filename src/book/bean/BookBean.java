/**
 * 書籍情報をまとめたBean
 * 脳死で書籍情報をまとめてgetter, setterを設定しただけなので問題がある場合は教えてください
 * メソッドの追加は問題ないと思います
 * 修正してくれてもいいです。
 * @author chibatoshinori
 */

package book.bean;

import java.io.Serializable;
import java.sql.Date;

public class BookBean implements Serializable {
	/** 図書ID */
	private int bookId;

	/** ISBN */
	private String isbn;

	/** 書名 */
	private String bookName;

	/** 著者 */
	private String author;

	/** 分類コード */
	private int categoryCode;

	/** 分類名 */
	private String categoryName;

	/** 出版社コード */
	private int publisherCode;

	/** 出版社名  */
	private String publisherName;

	/** 出版日 */
	private Date publicationDay;

	/** 入荷日 */
	private Date arrivalDate;

	/** 廃棄日 */
	private Date disposalDate;

	/** 備考 */
	private String remark;

	public BookBean() {

	}
	public BookBean(int bookId, String isbn, String bookName, String author, int categoryCode, String categoryName, int publisherCode,String publisherName, Date publicationDay, Date arrivalDate, Date disposalDate, String remark) {
		setBookId(bookId);
		setIsbn(isbn);
		setBookName(bookName);
		setAuthor(author);
		setCategoryCode(categoryCode);
		setCategoryName(categoryName);
		setPublisherCode(publisherCode);
		setPublisherName(publisherName);
		setPublicationDay(publicationDay);
		setArrivalDate(arrivalDate);
		setDisposalDate(disposalDate);
		setRemark(remark);
	}

	public BookBean(int bookId, String isbn, String categoryName, String bookName, String author, String publisherName, Date publicationDay, Date arrivalDate, Date disposalDate, String remark) {
		setBookId(bookId);
		setIsbn(isbn);
		setCategoryName(categoryName);
		setBookName(bookName);
		setAuthor(author);
		setPublisherName(publisherName);
		setPublicationDay(publicationDay);
		setArrivalDate(arrivalDate);
		setDisposalDate(disposalDate);
		setRemark(remark);
	}

	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public int getPublisherCode() {
		return publisherCode;
	}
	public void setPublisherCode(int publisherCode) {
		this.publisherCode = publisherCode;
	}
	public String getPublisherName() {
		return publisherName;
	}
	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}
	public Date getPublicationDay() {
		return publicationDay;
	}
	public void setPublicationDay(Date publicationDay) {
		this.publicationDay = publicationDay;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public Date getDisposalDate() {
		return disposalDate;
	}
	public void setDisposalDate(Date disposalDate) {
		this.disposalDate = disposalDate;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
