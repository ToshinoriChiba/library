/**
 * 図書の廃棄を完了するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookStateBean;
import book.dao.BookStateDAO;
import exception.DAOException;

/**
 * Servlet implementation class BookDeleteCompleteServlet
 */
@WebServlet("/BookDeleteCompleteServlet")
public class BookDeleteCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookDeleteCompleteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		BookStateBean bean = (BookStateBean) session.getAttribute("book");
		try {
			if (bean != null) {
				BookStateDAO dao = new BookStateDAO();
				BookStateBean confirm = dao.selectByBookId(bean.getBookId());
				if (confirm != null && confirm.getDisposalDate() != null) {
					String error = "既に廃棄されています。";
					request.setAttribute("error", error);
					gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
					return;
				}
				Date today = new Date(System.currentTimeMillis());
				int id = bean.getBookId();
				BookStateBean book = new BookStateBean();
				book.setDisposalDate(today);
				dao.updateBookState(book, id);
				request.setAttribute("action", "disposal");
				gotoPage(request, response, "/WEB-INF/bookDeleteCompleteView.jsp");
			}
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
