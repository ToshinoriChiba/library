/**
 * 図書情報の編集を完了するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;
import book.dao.BookInfoDAO;
import exception.DAOException;

/**
 * Servlet implementation class BookEditCompleteServlet
 */
@WebServlet("/BookEditCompleteServlet")
public class BookEditCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookEditCompleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		// セッション領域からbeanを取得
		BookInfoBean book = (BookInfoBean) session.getAttribute("book");
		BookInfoBean bean = (BookInfoBean) session.getAttribute("bean");

		// セッション領域に格納されている値の破棄
		session.removeAttribute("book");
		session.removeAttribute("bean");

		// daoでデータベースの更新
		try {
			BookInfoDAO dao = new BookInfoDAO();
			// BookStateDAO sdao = new BookStateDAO();

			/* book_stateのデータを更新
			List<BookStateBean> list = sdao.selectByISBN(book.getIsbn());
			BookStateBean sbean = new BookStateBean();
			sbean.setIsbn(bean.getIsbn());
			sbean.setBookName(bean.getBookName());
			for (BookStateBean b : list) {
				sdao.updateBookState(sbean, b.getBookId());
			}*/
			int rows = dao.updateBookInfo(bean, book.getIsbn());
			request.setAttribute("rows", rows);

			gotoPage(request, response, "/WEB-INF/bookEditCompleteView.jsp");
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
