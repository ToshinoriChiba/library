/**
 * 図書追加完了のServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;
import book.bean.BookStateBean;
import book.dao.BookStateDAO;
import exception.DAOException;

/**
 * Servlet implementation class BookAddCompleteServlet
 */
@WebServlet("/BookAddCompleteServlet")
public class BookAddCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookAddCompleteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// 入荷個数を取得
		int num = Integer.parseInt(request.getParameter("num"));
		HttpSession session = request.getSession();
		BookInfoBean bean = (BookInfoBean) session.getAttribute("bean");

		Date today = new Date(System.currentTimeMillis());

		BookStateBean book = new BookStateBean(bean.getIsbn(), bean.getBookName(), today);
		BookStateDAO dao;
		try {
			dao = new BookStateDAO();
			for (int i=0; i < num; i++) {
				dao.insertBookState(book);
			}
			gotoPage(request, response, "/WEB-INF/bookAddCompleteView.jsp");
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
