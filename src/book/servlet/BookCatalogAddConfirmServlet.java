/**
 * リクエストに格納されている図書情報を取得するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;

/**
 * Servlet implementation class BookCatalogAddConfirmServlet
 */
@WebServlet("/BookCatalogAddConfirmServlet")
public class BookCatalogAddConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookCatalogAddConfirmServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストから各種値を取得
		String isbn = request.getParameter("isbn");
		int categoryCode = Integer.parseInt(request.getParameter("category"));
		String bookName = request.getParameter("bookName");
		String author = request.getParameter("author");
		int publisher = Integer.parseInt(request.getParameter("publisher"));
		String publication = request.getParameter("publication");
		Date publicationDay = null;

		if (!publication.equals("")) {
			publicationDay = Date.valueOf(publication);
			Date today = new Date(System.currentTimeMillis());
			if (publicationDay.after(today)) {
				String error = "出版日の値が不正です。本日より前の日付けを入力してください。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
		}
		BookInfoBean bean = new BookInfoBean(isbn, categoryCode, publisher, bookName, author, publicationDay);

		// セッション領域に格納
		HttpSession session = request.getSession();
		session.setAttribute("bean", bean);

		gotoPage(request, response, "/WEB-INF/BookCatalogConfirmView.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
