/**
 * 目録追加を完了するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;
import book.dao.BookInfoDAO;
import exception.DAOException;

/**
 * Servlet implementation class BookCatalogAddCompleteServlet
 */
@WebServlet("/BookCatalogAddCompleteServlet")
public class BookCatalogAddCompleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookCatalogAddCompleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		BookInfoBean bean = (BookInfoBean) session.getAttribute("bean");
		try {
			BookInfoDAO dao = new BookInfoDAO();
			// 既に同じISBNが存在していないかのチェック
			BookInfoBean confirm = dao.selectByISBN(bean.getIsbn());
			if (confirm !=null) {
				String error = "既に目録に追加されています。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
			dao.insertBookInfo(bean);
			gotoPage(request, response, "/WEB-INF/bookCatalogAddCompleteView.jsp");
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
