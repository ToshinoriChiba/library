/**
 * 図書の入荷か目録追加かを判断するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;
import book.dao.BookInfoDAO;
import exception.DAOException;

/**
 * Servlet implementation class BookAddServlet
 */
@WebServlet("/BookAddServlet")
public class BookAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookAddServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// 入力されたISBN番号の取り出し
		String isbn = request.getParameter("isbn");

		// BookInfoDAOで対応するISBN番号の図書があるか検索
		try {
			BookInfoDAO dao = new BookInfoDAO();
			BookInfoBean bean = dao.selectByISBN(isbn);
			if (bean != null) {
				HttpSession session = request.getSession();
				session.setAttribute("bean", bean);
				gotoPage(request, response, "/WEB-INF/bookAddSearchResultView.jsp");
			} else {
				gotoPage(request, response, "/WEB-INF/bookCatalogAddView.jsp");
			}
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
