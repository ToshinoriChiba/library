/**
 * 図書廃棄日付をDBに入力するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookStateBean;
import book.dao.BookStateDAO;
import exception.DAOException;

/**
 * Servlet implementation class BookDeleteServlet
 */
@WebServlet("/BookDeleteServlet")
public class BookDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookDeleteServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// 図書IDの取得
		int id = Integer.parseInt(request.getParameter("bookId"));

		try {
			BookStateDAO dao = new BookStateDAO();
			BookStateBean bean = dao.selectByBookId(id);

			if (bean == null) {
				String error = "入力された図書IDに対応する図書が存在しません。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
			}

			// 取得したBookStateBeanをセッションスコープに入れる
			HttpSession session = request.getSession();
			session.setAttribute("book", bean);

			gotoPage(request, response, "/WEB-INF/bookDeleteSearchResultView.jsp");
		} catch (DAOException e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
