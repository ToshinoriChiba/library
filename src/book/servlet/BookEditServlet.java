/**
 * ISBNb番号を取得して存在するかどうかを調べた後にあった場合は検索結果画面に遷移させるServlet
 * @author chibatoshinori
 */
package book.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;
import book.dao.BookInfoDAO;

/**
 * Servlet implementation class BookEditServlet
 */
@WebServlet("/BookEditServlet")
public class BookEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookEditServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストの文字コードを指定
		request.setCharacterEncoding("UTF-8");
		// リクエストから検索に使うbookIdを取得
		String isbn = request.getParameter("isbn");
		//

		try {
			BookInfoDAO dao = new BookInfoDAO();
			BookInfoBean bean = dao.selectByISBN(isbn);
			if (bean == null) {
				String error = "入力されたISBN番号に対応する図書目録が存在しません。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			} else {
				// セッションスコープに入れる
				HttpSession session = request.getSession();
				session.setAttribute("book", bean);
				// ディスパッチ
				gotoPage(request, response, "/WEB-INF/bookEditSelectView.jsp");
				return;
			}
		} catch (Exception e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}
}
