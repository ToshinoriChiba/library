/**
 * 入力された情報を取得してBeanに格納し、確認画面に遷移するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import book.bean.BookInfoBean;
import book.dao.BookInfoDAO;

/**
 * Servlet implementation class BookEditConfirmServlet
 */
@WebServlet("/BookEditConfirmServlet")
public class BookEditConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookEditConfirmServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// 各パラメータの取得
		String isbn = request.getParameter("isbn");
		int categoryCode = Integer.parseInt(request.getParameter("category"));
		String bookName = request.getParameter("bookName");
		String author = request.getParameter("author");
		int publisherCode = Integer.parseInt(request.getParameter("publisher"));
		String publication = request.getParameter("publication");
		Date publicationDay = null;

		if (!publication.equals("")) {
			publicationDay = Date.valueOf(publication);
			Date today = new Date(System.currentTimeMillis());
			if (publicationDay.after(today)) {
				String error = "出版日の値が不正です。本日より前の日付けを入力してください。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
		}

		try {
			BookInfoDAO dao = new BookInfoDAO();
			BookInfoBean bean = dao.selectByISBN(isbn);
			if (bean != null) {
				// すでにそのISBNの本が存在するのでエラー画面に遷移
				String error = "既に指定されたISBN番号の目録情報が存在するため変更することができません";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			}
		} catch (Exception e){
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
			return;
		}
		// BookInfoBeanに格納
		BookInfoBean bean = new BookInfoBean(isbn, categoryCode, publisherCode, bookName, author, publicationDay);
		// セッション領域に格納
		HttpSession session = request.getSession();
		session.setAttribute("bean", bean);
		// 確認画面へ遷移
		gotoPage(request, response, "/WEB-INF/bookEditConfirmView.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
