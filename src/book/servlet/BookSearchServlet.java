/**
 * 図書を検索するServlet
 * @author chibatoshinori
 */

package book.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import book.bean.BookBean;
import book.bean.BookInfoBean;
import book.bean.BookStateBean;
import book.dao.BookInfoDAO;
import book.dao.BookStateDAO;

/**
 * Servlet implementation class BookSearchServlet
 */
@WebServlet("/BookSearchServlet")
public class BookSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookSearchServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードの指定
		request.setCharacterEncoding("UTF-8");

		try {
			BookStateDAO stateDAO = new BookStateDAO();
			BookInfoDAO infoDAO = new BookInfoDAO();
			BookStateBean bean_state = stateDAO.selectByBookId(Integer.parseInt(request.getParameter("bookId")));
			if (bean_state == null) {
				// 該当するレコードが存在しない場合
				String error = "検索条件に該当するデータが存在しません。";
				request.setAttribute("error", error);
				gotoPage(request, response, "/WEB-INF/rentalerror.jsp");
				return;
			} else {
				BookInfoBean bean_info = infoDAO.selectByISBN(bean_state.getIsbn());

				int bookId = bean_state.getBookId();
				String isbn = bean_info.getIsbn();
				String category = bean_info.getCategoryName();
				String bookName = bean_info.getBookName();
				String author = bean_info.getAuthor();
				String publisher = bean_info.getPublisherName();
				Date publicationDay = bean_info.getPublicationDay();
				Date arrival = bean_state.getArrivalDate();
				Date disposal = bean_state.getDisposalDate();
				String remark = bean_state.getRemark();

				BookBean bean = new BookBean(bookId, isbn, category, bookName, author, publisher, publicationDay, arrival, disposal, remark);

				request.setAttribute("book", bean);

				gotoPage(request, response, "/WEB-INF/bookSearchResultView.jsp");
			}
		} catch (Exception e) {
			gotoPage(request, response, "/WEB-INF/error.jsp");
			e.printStackTrace();
		}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);
		rd.forward(request, response);
	}

}
