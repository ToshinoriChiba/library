drop database library;
create database library;

\c library

CREATE TABLE userinf
(
  user_id serial NOT NULL,
  user_family_name varchar(10) NOT NULL,
  user_name varchar(10) NOT NULL,
  user_birthday date NOT NULL,
  user_tel varchar(20) NOT NULL,
  user_email varchar(100) UNIQUE NOT NULL,
  user_postal char(7) NOT NULL,
  user_address varchar(100) NOT NULL,
  join_day date,
  leave_day date,
  CONSTRAINT user_id PRIMARY KEY (user_id)
);





CREATE TABLE publisher
(
  publisher_code integer NOT NULL,
  publisher_name character varying(20) NOT NULL,
  CONSTRAINT publisher_pkey PRIMARY KEY (publisher_code)
);



CREATE TABLE category
(
  category_code integer NOT NULL,
  category_name character varying(20) NOT NULL,
  CONSTRAINT category_pkey PRIMARY KEY (category_code)
);



CREATE TABLE book_info
(
  book_info_isbn character varying(13) NOT NULL,
  category_code integer NOT NULL,
  publisher_code integer NOT NULL,
  book_info_name character varying(100) NOT NULL,
  book_info_author character varying(20) NOT NULL,
  publication_day date NOT NULL,
  CONSTRAINT book_info_pkey PRIMARY KEY (book_info_isbn),
  foreign key (category_code) references category (category_code),
  foreign key (publisher_code) references publisher (publisher_code)
);




CREATE TABLE book_state
(
  book_state_id serial NOT NULL,
  book_info_isbn character varying(13) NOT NULL,
  book_info_name character varying(20) NOT NULL,
  arrival_date date NOT NULL DEFAULT current_date,
  disposal_date date,
  remark character varying(100),
  CONSTRAINT book_state_pkey PRIMARY KEY (book_state_id),
  foreign key (book_info_isbn) references book_info (book_info_isbn)
);






CREATE TABLE rental
(
  rental_id serial NOT NULL,
  book_state_id integer NOT NULL,
  user_id integer NOT NULL,
  rental_day date NOT NULL DEFAULT current_date,
  limit_day date NOT NULL,
  return_day date,
  remark character varying(100),
  CONSTRAINT rental_id PRIMARY KEY (rental_id),
  foreign key (user_id) references userinf (user_id),
  foreign key (book_state_id) references book_state (book_state_id)
);




insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day) values('山田','圭吾','1990/03/12','09084958560','11112@gmail.com','333-788','新宿','2010/05/05','2011/05/05');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address) values('相田','真紀','1991/08/22','09084558560','11113@gmail.com','333-766','大阪');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day) values('愛田','美沙','1992/07/12','09084468560','11114@gmail.com','333-711','名古屋','2010/05/05');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address) values('相原','里奈','1993/09/23','09084753560','11115@gmail.com','333-718','広島');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day) values('相川','涼','1989/08/25','09084914590','11116@gmail.com','333-779','新宿','2011/07/05');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day) values('青木','未央','1995/12/15','09082985560','11117@gmail.com','333-497','大阪','2012/05/05','2017/07/05');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address) values('青木','佐和子','1988/09/14','09985258560','11118@gmail.com','333-787','名古屋');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day) values('青葉','琴美','1985/04/19','09081495560','11119@gmail.com','333-707','日光','2013/08/05','2013/04/09');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address, join_day, leave_day) values('安藤','望','1999/03/18','09019638560','11121@gmail.com','333-852','新宿','2011/05/05','2011/06/05');
insert into userinf(user_family_name, user_name, user_birthday, user_tel, user_email, user_postal, user_address) values('安西','久美','1982/04/13','09084149560','12111@gmail.com','333-704','新宿');


insert into publisher values(1,'出版社a');
insert into publisher values(2,'出版社b');
insert into publisher values(3,'出版社c');
insert into publisher values(4,'出版社d');
insert into publisher values(5,'出版社e');
insert into publisher values(6,'出版社f');
insert into publisher values(7,'出版社g');
insert into publisher values(8,'出版社h');
insert into publisher values(9,'出版社j');
insert into publisher values(10,'出版社k');



insert into category values(0,'総記');
insert into category values(1,'哲学');
insert into category values(2,'歴史');
insert into category values(3,'社会科学');
insert into category values(4,'自然科学');
insert into category values(5,'技術');
insert into category values(6,'産業');
insert into category values(7,'芸術');
insert into category values(8,'言語');
insert into category values(9,'文学');



insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('0123456789012',1,1,'java1','著者1','2010/07/05');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('1234567890123',2,2,'java2','著者2','2009/04/05');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('2345678901234',6,10,'java3','著者3','2011/04/08');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('3456789012345',1,9,'java4','著者4','2015/01/04');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('4567890123456',5,8,'java5','著者5','2011/08/01');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('5678901234567',0,1,'java6','著者6','2012/09/09');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('6789012345678',2,4,'java7','著者7','2011/07/01');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('7890123456789',8,2,'java8','著者8','2014/06/09');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('8901234567890',9,4,'java9','著者9','2010/12/04');
insert into book_info(book_info_isbn, category_code, publisher_code, book_info_name, book_info_author, publication_day) values('9012345678901',1,1,'java10','著者10','2011/07/09');




insert into book_state(book_info_isbn, book_info_name, arrival_date) values('0123456789012','java1','2011/07/09');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('1234567890123','java2','2011/07/10');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('2345678901234','java3','2011/07/11');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('3456789012345','java4','2011/07/12');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('4567890123456','java5','2011/07/13');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('5678901234567','java6','2011/07/14');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('6789012345678','java7','2011/07/15');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('7890123456789','java8','2011/07/16');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('8901234567890','java9','2011/07/17');
insert into book_state(book_info_isbn, book_info_name, arrival_date) values('9012345678901','java10','2011/07/19');




insert into rental values(1,1,1,'2011/07/09','2011/07/15');
insert into rental values(2,3,5,'2011/07/19','2011/07/21');
insert into rental values(3,6,9,'2011/07/29','2011/08/11');
insert into rental values(4,7,8,'2011/07/28','2011/08/12');
insert into rental values(5,5,5,'2011/07/15','2011/07/30');
insert into rental values(6,4,6,'2011/07/18','2011/07/29');
insert into rental values(7,3,4,'2011/07/15','2011/07/19');
insert into rental values(8,9,1,'2011/07/14','2011/07/19');
insert into rental values(9,7,2,'2011/07/11','2011/07/19');
insert into rental values(10,8,7,'2011/07/05','2011/07/10');
