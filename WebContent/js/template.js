/**
 * @author Chiba Toshinori
 */

function footer(){
	var html = "";
	html += '<p>〒104-0061 東京都中央区銀座２丁目９−１３<br>';
	html += 'Tel : 03-0123-9876</p>';
	html += '<p><small> © 2018 新宿図書館 </small></p>';
	document.write(html);
}

function sidebar(){
	var html = "";
	html += '<a href="index.html">トップページ</a><br>';
	html += '<a href="userManage.html">会員管理</a><br>';
	html += '<a href="bookManagement.html">図書管理</a><br>';
	html += '<a href="RentalServlet?action=top">貸出・返却</a><br>'
	document.write(html);
}

