<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="book.bean.BookStateBean"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>図書削除検索結果</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">
			以下の図書を削除しますか?
			<form action="/library/BookDeleteCompleteServlet" method="get">
			<table border="1">
				<tbody>
					<tr>
						<td></td>
						<td>図書ID</td>
						<td>ISBN</td>
						<td>書名</td>
						<td>入荷日</td>
						<td>廃棄日</td>
						<td>備考</td>
					</tr>

					<tr>
						<td>
							<input type="submit" value="廃棄">
						</td>
						<td>${book.bookId }</td>
						<td>${book.isbn }</td>
						<td>${book.bookName}</td>
						<td>${book.arrivalDate }</td>
						<td>${book.disposalDate }</td>
						<td>${book.remark }</td>
					</tr>
				</tbody>
			</table>
			<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
            	sidebar();
            </script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
   		</script>
	</footer>

</body>
</html>
