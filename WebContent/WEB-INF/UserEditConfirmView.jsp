<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html">
<html>
<head>
<meta http-equiv="UTF-8">
<title>会員情報編集確認画面</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
</body>
<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">

		<div class="main">
		<h2>会員情報の編集</h2>
			以下のように書き換えます。よろしいですか?
			<form action="/library/UserEditCompleteServlet" method="get">
				<table border>
					<tbody>
						<tr>
							<td></td>
							<td>変更前</td>
							<td>変更後</td>
						</tr>
						<tr>
							<td>利用者苗字</td>
							<td>${bean.familyName }</td>
							<td>${user.familyName }</td>
						</tr>
						<tr>
							<td>利用者名前</td>
							<td>${bean.name}</td>
							<td>${user.name}</td>
						</tr>
						<tr>
							<td>利用者生年月日</td>
							<td>${bean.birthday}</td>
							<td>${user.birthday}</td>
						</tr>
						<tr>
							<td>利用者電話番号</td>
							<td>${bean.tel}</td>
							<td>${user.tel}</td>
						</tr>
						<tr>
							<td>利用者メールアドレス</td>
							<td>${bean.email}</td>
							<td>${user.email }</td>
						</tr>
						<tr>
							<td>利用者郵便番号</td>
							<td>${bean.postal  }</td>
							<td>${user.postal  }</td>
						</tr>
						<tr>
							<td>利用者住所</td>
							<td>${bean.address }</td>
							<td>${user.address  }</td>
						</tr>

					</tbody>
				</table>
				<input type="submit" value="変更">
				<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>








</body>
</html>