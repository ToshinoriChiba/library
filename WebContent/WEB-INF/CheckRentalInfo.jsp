<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="user.bean.UserBean" import="book.bean.BookBean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<% String act=(String)session.getAttribute("act"); %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>情報確認</title>
    <link href="/library/style.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="/library/js/template.js"></script>
</head>
<body>
<!--ヘッダー-->
<header>
        <h1>図書管理システム</h1>
    </header>

    <div class="container">
        <div class="main">
        <%	String link = "";
			String button="";
			String pos = "";
			if(act.equals("len")){
				link="LendingServlet";
				pos="貸出内容確認";
				button="貸出";
			}
			else if(act.equals("rec")){
				link="RecieveServlet";
				pos="返却内容確認";
				button="返却";
			}
			else{

			}
		%>
		<h2><%= pos %></h2><br>


		<h3>会員情報</h3><br>
        <table border="1">
				<tbody>
					<tr>

						<td>会員ID</td>
						<td>姓</td>
						<td>名</td>
						<td>誕生日</td>
						<td>電話番号</td>
						<td>EMail</td>
						<td>郵便番号</td>
						<td>住所</td>
						<td>入会日</td>
						<td>退会日</td>
					</tr>

					<tr>
						<td>${user.userId}</td>
						<td>${user.familyName}</td>
						<td>${user.name}</td>
						<td>${user.birthday}</td>
						<td>${user.tel}</td>
						<td>${user.email}</td>
						<td>${user.postal}</td>
						<td>${user.address}</td>
						<td>${user.joinDay}</td>
						<td>${user.leaveDay}</td>
					</tr>


				</tbody>

			</table>
			<form action="/library/<%= link %>?action=finish" method="post">
			<h3>図書情報</h3><br>
			<table border="1">
				<tbody>
					<tr>
						<td>図書ID</td>
						<td>ISBN</td>
						<td>図書名</td>
						<td>著者</td>
						<td>分類</td>
						<td>出版社</td>
						<td>出版日</td>
						<td>入荷日</td>
						<td>廃棄日</td>
						<td>備考</td>
						<%if(act.equals("len")) {%>
						<td>貸出返却備考</td>
						<%}else{} %>
					</tr>

					<%int i=1; %>
					<c:forEach items="${list}" var="value">
					<tr>
						<td>${value.bookId}</td>
						<td>${value.isbn}</td>
						<td>${value.bookName}</td>
						<td>${value.author}</td>
						<td>${value.categoryName}</td>
						<td>${value.publisherName}</td>
						<td>${value.publicationDay}</td>
						<td>${value.arrivalDate}</td>
						<td>${value.disposalDate}</td>
						<td>${value.remark}</td>
						<%if(act.equals("len")) {%>
						<td><input type="text" name="rem<%=i%>"></td>
						<%}else{} %>
					</tr>
					<%i++; %>
					</c:forEach>
				</tbody>
			</table>
			<input type="submit" value="<%= button %>">
			</form>

        <input type="button" onclick="location.href='/library/RentalServlet?action=<%=act%>'"value="キャンセル">
        </div>
        <div class="sidebar">
            <script type="text/javascript">
            	sidebar();
            </script>
        </div>
    </div>
    <!--/.container-->

    <footer id="footer">
    	<script type="text/javascript" >
			footer();
   		</script>
    </footer>
</body>
</html>
