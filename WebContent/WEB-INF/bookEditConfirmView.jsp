<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>図書情報編集確認画面</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">

		<div class="main">
		<h2>図書情報の編集</h2>
			以下のように書き換えます。よろしいですか?
			<form action="/library/BookEditCompleteServlet" method="get">
				<table border="1">
					<tbody>
						<tr>
							<td></td>
							<td>変更前</td>
							<td>変更後</td>
						</tr>
						<tr>
							<td>ISBN</td>
							<td>${book.isbn }</td>
							<td>${bean.isbn }</td>
						</tr>
						<tr>
							<td>分類</td>
							<td>${book.categoryName }</td>
							<td>${bean.categoryName }</td>
						</tr>
						<tr>
							<td>書名</td>
							<td>${book.bookName }</td>
							<td>${bean.bookName }</td>
						</tr>
						<tr>
							<td>著者</td>
							<td>${book.author }</td>
							<td>${bean.author }</td>
						</tr>
						<tr>
							<td>出版社</td>
							<td>${book.publisherName }</td>
							<td>${bean.publisherName }</td>
						</tr>
						<tr>
							<td>出版日</td>
							<td>${book.publicationDay }</td>
							<td>${bean.publicationDay }</td>
						</tr>

					</tbody>
				</table>
				<input type="submit" value="変更">
				<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>
</body>
</html>
