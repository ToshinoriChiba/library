<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="user.bean.UserBean" %>
<%@ page import="user.dao.UserDAO" %>

<!DOCTYPE html">
<html>
<head>
<meta http-equiv="UTF-8">
<title>会員情報変更</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script>
  $(function() {
    $(".datepicker").datepicker({
        maxDate: '0y', // 今日まで
        changeYear: true, // 表示年の指定が可能
        changeMonth: true, // 表示月の指定が可能
        dateFormat: 'yy-mm-dd' // 年-月-日
    });
  });
</script>
</head>
<body>

<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">

		<div class="main">
		<h2>会員情報の編集</h2>
			会員情報を以下の表で変更できます。
			<form action="/library/UserEditConfirmServlet" method="post">
				<table border="1">
					<tbody>
						<tr>
							<td></td>
							<td>変更前</td>
							<td>変更後</td>
						</tr>
						<tr>
							<td>利用者苗字</td>
							<td>${bean.familyName}</td>
							<td><input type="text" name="familyName" maxlength="10"></td>
						</tr>
						<tr>
							<td>利用者名前</td>
							<td>${bean.name }</td>
							<td><input type="text" name="name" maxlength="10"></td>
						</tr>
						<tr>
							<td>利用者生年月日</td>
							<td>${bean.birthday}</td>
							<td><input type="text" name="birthday" class="datepicker" pattern="^(\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$"></td>
						</tr>
						<tr>
							<td>利用者電話番号</td>
							<td>${bean.tel }</td>
							<td><input type="text" name="tel" pattern="^[0-9]+$" maxlength="20"></td>
						</tr>
						<tr>
							<td>利用者メールアドレス</td>
							<td>${bean.email }</td>
							<td><input type="email" name="email" maxlength="100"></td>
						</tr>
						<tr>
							<td>利用者郵便番号</td>
							<td>${bean.postal }</td>
							<td><input type="text" name="postal" maxlength="7" pattern="^[0-9]+$"></td>
						</tr>
						<tr>
							<td>利用者住所</td>
							<td>${bean.address }</td>
							<td><input type="text" name="address" maxlength="100"></td>
						</tr>

					</tbody>
				</table>
				<input type="submit" value="確認画面へ">
				<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>
</body>
</html>