<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="rental.bean.RentalBean"%>
<%List<RentalBean> data = (List<RentalBean>) request.getAttribute("data"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>検索結果</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/library/js/template.js">
</script>
</head>
<body>
<!--ヘッダー-->
<header>
        <h1>図書管理システム</h1>
    </header>

    <div class="container">
        <div class="main">


			<%if(data.size()==0){%>
				<h3>入力情報に適合するデータは存在しません。</h3>
			<%}
			else{%>
			<h3>検索結果</h3>
			<table border="1">
				<tbody>
					<tr>
						<td>貸出ID</td>
						<td>図書ID</td>
						<td>会員ID</td>
						<td>貸出日</td>
						<td>返却期日</td>
						<td>返却日</td>
						<td>備考</td>
					</tr>
					<c:forEach items="${data}" var="value">
					<tr>
						<td>${value.rentalId}</td>
						<td>${value.bookStateId}</td>
						<td>${value.userId}</td>
						<td>${value.rentalDay}</td>
						<td>${value.limitDay}</td>
						<td>${value.returnDay}</td>
						<td>${value.remark}</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<%} %>
        </div>
        <div class="sidebar">
            <script type="text/javascript">
            	sidebar();
            </script>
        </div>
    </div>
    <!--/.container-->

    <footer id="footer">
    	<script type="text/javascript" >
			footer();
   		</script>
    </footer>
</body>
</html>
