<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>図書入荷画面</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>
<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">

			<form action="/library/BookAddCompleteServlet" method="get">
			<table border="1">
				<tbody>
					<tr>
						<td>ISBN</td>
						<td>分類</td>
						<td>書名</td>
						<td>著者</td>
						<td>出版社</td>
						<td>出版日</td>
					</tr>

					<tr>
						<td>${bean.isbn }</td>
						<td>${bean.categoryCode}</td>
						<td>${bean.bookName}</td>
						<td>${bean.author }</td>
						<td>${bean.publisherCode }</td>
						<td>${bean.publicationDay }</td>
					</tr>
				</tbody>
			</table>
			入荷する個数 :
			<input type="text" name="num" pattern="^[0-9]+$">
			<input type="submit" value="入荷">
			<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>

</body>
</html>
