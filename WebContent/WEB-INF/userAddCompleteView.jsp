<%-- @author 清水夏美 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員追加完了</title>
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>

	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">
			<h3>会員の追加が完了しました。</h3>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
            	sidebar();
            </script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
				footer();
   			</script>
	</footer>

</body>
</html>