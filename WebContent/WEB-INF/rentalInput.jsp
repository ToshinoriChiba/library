<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% String act=(String)request.getAttribute("act"); %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>情報入力</title>
    <link href="/library/style.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="/library/js/template.js"></script>
</head>
<body>
<!--ヘッダー-->
<header>
        <h1>図書管理システム</h1>
    </header>

    <div class="container">
        <div class="main">

		<%	String link = "";
			String button="確認画面へ";
			String pos = "";
			if(act.equals("len")){
				link="LendingServlet?action=check";
				pos="貸出記録";
			}
			else if(act.equals("rec")){
				link="RecieveServlet?action=check";
				pos="返却記録";
			}
			else if(act.equals("his")){
				link="SearchServlet";
				pos="履歴検索";
				button="検索";
			}
			else{

			}
		%>

		<h2><%= pos %></h2><br>

        <form action="/library/<%= link %>" method="post">
		会員ID:<input type="text" name="userId"><br>

        	<%
        	if(act.equals("len")||act.equals("rec")){
        	%>
        		<br>図書ID:<input type="text" name="bookId1"><br>
        		図書ID:<input type="text" name="bookId2"><br>
        		図書ID:<input type="text" name="bookId3"><br>
        		図書ID:<input type="text" name="bookId4"><br>
        		図書ID:<input type="text" name="bookId5"><br><br>
        	<%
        	}
        	else{
        	%>
        		図書ID:<input type="text" name="bookId0"><br><br>
        <%
        	}
       	 %>

       	 <input type="submit" value="<%= button %>">
        </form>
        </div>
        <div class="sidebar">
            <script type="text/javascript">
            	sidebar();
            </script>
        </div>
    </div>
    <!--/.container-->

    <footer id="footer">
    	<script type="text/javascript" >
			footer();
   		</script>
    </footer>
</body>
</html>
