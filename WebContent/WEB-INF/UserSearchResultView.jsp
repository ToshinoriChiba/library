<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="user.bean.UserBean"%>



<!DOCTYPE html">
<html>
<head>
<meta http-equiv="UTF-8">
<title>検索結果</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">
		<h3>会員検索結果</h3>
		以下の会員が検索されました。
			<table border="1">


					<tr>
						<td>利用者ID</td>
						<td>利用者苗字</td>
						<td>利用者名前</td>
						<td>利用者郵便番号</td>
						<td>利用者住所</td>
						<td>利用者電話番号</td>
						<td>利用者メールアドレス</td>
						<td>利用者生年月日</td>
						<td>入会日</td>
						<td>退会日</td>

					</tr>
					<tr>
						<td>${bean.userId }</td>
						<td>${bean.familyName }</td>
						<td>${bean.name }</td>
						<td>${bean.postal}</td>
						<td>${bean.address}</td>
						<td>${bean.tel}</td>
						<td>${bean.email}</td>
						<td>${bean.birthday}</td>
						<td>${bean.joinDay}</td>
						<td>${bean.leaveDay}</td>
					</tr>


			</table>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
            	sidebar();
            </script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
   		</script>
	</footer>



</body>
</html>