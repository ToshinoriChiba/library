<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% String message="正常に返却処理が完了しました"; %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>返却完了</title>
    <link href="/library/style.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="/library/js/template.js"></script>
</head>
<body>
<!--ヘッダー-->
<header>
        <h1>図書管理システム</h1>
    </header>

    <div class="container">
        <div class="main">

		<%=message %>

        </div>
        <div class="sidebar">
            <script type="text/javascript">
            	sidebar();
            </script>
        </div>
    </div>
    <!--/.container-->

    <footer id="footer">
    	<script type="text/javascript" >
			footer();
   		</script>
    </footer>
</body>
</html>
