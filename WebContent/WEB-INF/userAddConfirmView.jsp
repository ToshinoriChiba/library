<%-- @author 清水夏美 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員追加確認画面</title>
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>

</head>
<body>

	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">
			<h3>追加する会員の確認</h3>
			<p>次の会員を追加します。よろしいですか？</p>
			<table border="1">
				<tbody>
					<tr>
						<td>氏名</td>
						<td>郵便番号</td>
						<td>住所</td>
						<td>TEL</td>
						<td>E-Mail</td>
						<td>生年月日</td>
						<td>入会日</td>
					</tr>
					<tr>
						<td>${user.familyName}${user.name }</td>
						<td>${user.postal}</td>
						<td>${user.address}</td>
						<td>${user.tel}</td>
						<td>${user.email}</td>
						<td>${user.birthday}</td>
						<td>${user.joinDay }</td>
					</tr>
				</tbody>
			</table>
			<form action="/library/UserAddCompleteServlet">
				<input type="submit" value="追加"> <input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>
</body>
</html>