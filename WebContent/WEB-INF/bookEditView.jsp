<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="book.bean.CategoryBean"%>
<%@ page import="book.dao.CategoryDAO"%>
<%@ page import="book.bean.PublisherBean"%>
<%@ page import="book.dao.PublisherDAO"%>

<%
	CategoryDAO catedao = new CategoryDAO();
	List<CategoryBean> catelist = catedao.selectCategoryAll();
	request.setAttribute("catelist", catelist);

	PublisherDAO pubdao = new PublisherDAO();
	List<PublisherBean> publist = pubdao.selectPublisherAll();
	request.setAttribute("publist", publist);
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>図書情報編集</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">

<script>
	$(function() {
		$("#datepicker").datepicker({
			maxDate : '0y', // 今日まで
			changeYear : true, // 表示年の指定が可能
			changeMonth : true, // 表示月の指定が可能
			dateFormat : 'yy-mm-dd' // 年-月-日
		});
	});
</script>
</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">

		<div class="main">
			<h2>図書情報の編集</h2>
			図書情報を編集ができます。
			<form action="/library/BookEditConfirmServlet" method="get">
				<table border="1">
					<tbody>
						<tr>
							<td></td>
							<td>変更前</td>
							<td>変更後</td>
						</tr>
						<tr>
							<td>ISBN</td>
							<td>${book.isbn }</td>
							<td><input type="text" name="isbn" pattern="^[0-9]+$"
								maxlength="13"></td>
						</tr>
						<tr>
							<td>分類</td>
							<td>${book.categoryName }</td>
							<td><select name="category">
									<c:forEach items="${catelist }" var="cate">
										<option value="${cate.categoryCode }">${cate.categoryCode }.
											${cate.categoryName }</option>
									</c:forEach>
							</select></td>
						</tr>
						<tr>
							<td>書名</td>
							<td>${book.bookName }</td>
							<td><input type="text" name="bookName" maxlength="100"></td>
						</tr>
						<tr>
							<td>著者</td>
							<td>${book.author }</td>
							<td><input type="text" name="author" maxlength="20"></td>
						</tr>
						<tr>
							<td>出版社</td>
							<td>${book.publisherName }</td>
							<td><select name="publisher">
									<c:forEach items="${publist }" var="pub">
										<option value="${pub.publisherCode }">${pub.publisherCode }.
											${pub.publisherName }</option>
									</c:forEach>
							</select></td>
						</tr>
						<tr>
							<td>出版日</td>
							<td>${book.publicationDay }</td>
							<td><input type="text" id="datepicker" name="publication"
								pattern="^(\d{4})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$">
							</td>
						</tr>

					</tbody>
				</table>
				<input type="submit" value="確認画面へ"> <input type="button"
					value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>
</body>
</html>
