<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="book.bean.BookStateBean"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>図書削除</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>

</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">
		<h3>
		<c:choose>
			<c:when test="${action eq 'delete' }">図書情報の削除に成功しました</c:when>
			<c:when test="${action eq 'disposal' }">廃棄日情報を更新しました</c:when>
			<c:when test="${action eq 'miss' }">情報の受け渡しに失敗した可能性があります</c:when>
		</c:choose>
		</h3>

		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>
</body>
</html>
