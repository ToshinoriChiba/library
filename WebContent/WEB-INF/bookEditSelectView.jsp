<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="book.bean.BookInfoBean" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>変更する図書の選択</title>

<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">
		<div class="main">

			<form action="BookSelectServlet" method="get">
			<table border="1">
				<tbody>
					<tr>
						<td></td>
						<td>ISBN</td>
						<td>分類</td>
						<td>書名</td>
						<td>著者</td>
						<td>出版社</td>
						<td>出版日</td>
					</tr>

					<tr>
						<td><input type="submit" value="編集"></td>
						<td>${book.isbn }</td>
						<td>${categoryName}</td>
						<td>${book.bookName}</td>
						<td>${book.author }</td>
						<td>${publisherName }</td>
						<td>${book.publicationDay }</td>
					</tr>
				</tbody>
			</table>
			<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>

</body>
</html>
