<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="user.bean.UserBean"%>


<!DOCTYPE html">
<html>
<head>
<meta http-equiv="UTF-8">
<title>退会表示</title>

<link href="/library/style.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="./js/template.js"></script>

</head>
<body>
<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>
	<div class="container">
		<div class="main">
		<h3>退会完了</h3>
			会員の退会処理が完了しました。
		</div>
		<div class="sidebar">
			<script type="text/javascript">
            	sidebar();
            </script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
   		</script>
	</footer>
</body>
</html>