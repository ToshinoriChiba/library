<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>図書目録追加確認画面</title>
<link href="/library/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/template.js"></script>
</head>
<body>
	<!--ヘッダー-->
	<header>
		<h1>図書管理システム</h1>
	</header>

	<div class="container">

		<div class="main">
		<h2>図書目録追加確認</h2>
		以下の図書目録を追加します。
			<form action="/library/BookCatalogAddCompleteServlet" method="get">
				<table border="1">
					<tbody>
						<tr>
							<td>ISBN</td>
							<td>${bean.isbn }</td>
						</tr>
						<tr>
							<td>分類</td>
							<td>${bean.categoryCode }</td>
						</tr>
						<tr>
							<td>書名</td>
							<td>${bean.bookName }</td>
						</tr>
						<tr>
							<td>著者</td>
							<td>${bean.author }</td>
						</tr>
						<tr>
							<td>出版社</td>
							<td>${bean.publisherCode }</td>
						</tr>
						<tr>
							<td>出版日</td>
							<td>${bean.publicationDay }</td>
						</tr>
					</tbody>
				</table>
				<input type="submit" value="追加する">
				<input type="button" value="戻る" onClick="history.back()">
			</form>
		</div>
		<div class="sidebar">
			<script type="text/javascript">
				sidebar();
			</script>
		</div>
	</div>
	<!--/.container-->

	<footer id="footer">
		<script type="text/javascript">
			footer();
		</script>
	</footer>
</body>
</html>
